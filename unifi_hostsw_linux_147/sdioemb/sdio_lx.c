/*
 * Linux kernel module support.
 *
 * Copyright (C) 2007-2008 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include <linux/module.h>
#include <linux/device.h>

#include "sdio_layer.h"

int sdd_card_poll_interval_ms = 500;
module_param_named(card_poll_interval, sdd_card_poll_interval_ms, int, 0644);
MODULE_PARM_DESC(card_poll_interval, "interval (in ms) between card detect polls");

int sdd_max_bus_freq = 0;
module_param_named(max_bus_freq, sdd_max_bus_freq, int, 0444);
MODULE_PARM_DESC(max_bus_freq, "maximum bus frequency (in Hz), or 0 for card's maximum");

int sdd_max_block_size = 0;
module_param_named(max_block_size, sdd_max_block_size, int, 0444);
MODULE_PARM_DESC(max_block_size, "maximum block size, or 0 for the card's maximum");

EXPORT_SYMBOL(sdio_driver_register);
EXPORT_SYMBOL(sdio_driver_unregister);
EXPORT_SYMBOL(sdio_set_block_size);
EXPORT_SYMBOL(sdio_set_max_bus_freq);
EXPORT_SYMBOL(sdio_set_bus_width);
EXPORT_SYMBOL(sdio_enable_function);
EXPORT_SYMBOL(sdio_disable_function);
EXPORT_SYMBOL(sdio_idle_function);
EXPORT_SYMBOL(sdio_enable_interrupt);
EXPORT_SYMBOL(sdio_disable_interrupt);
EXPORT_SYMBOL(sdio_cis_get_tuple);

EXPORT_SYMBOL(sdio_read8);
EXPORT_SYMBOL(sdio_write8);
EXPORT_SYMBOL(sdio_f0_read8);
EXPORT_SYMBOL(sdio_f0_write8);
EXPORT_SYMBOL(sdio_read);
EXPORT_SYMBOL(sdio_write);

EXPORT_SYMBOL(sdio_slot_alloc);
EXPORT_SYMBOL(sdio_slot_free);
EXPORT_SYMBOL(sdio_slot_register);
EXPORT_SYMBOL(sdio_slot_unregister);
EXPORT_SYMBOL(sdio_card_inserted);
EXPORT_SYMBOL(sdio_card_removed);
EXPORT_SYMBOL(sdio_hard_reset);
EXPORT_SYMBOL(sdio_interrupt);
EXPORT_SYMBOL(sdio_cmd_complete);

EXPORT_SYMBOL(sdio_power_on);
EXPORT_SYMBOL(sdio_power_off);
EXPORT_SYMBOL(sdio_suspend);
EXPORT_SYMBOL(sdio_resume);

/* Only for sdio_uif. */
EXPORT_SYMBOL(slot_set_bus_freq);

struct sdioe_dev {
    struct device dev;
};

struct bus_type sdioe_bus_type = {
    .name   = "sdioemb",
    /* Seems we can we get away with these being NULL since no drivers
       are ever added to the bus. */
    .match  = NULL,
    .probe  = NULL,
    .remove = NULL,
};

static void sdioe_device_release(struct device *dev)
{
    struct sdioe_dev *sdev = container_of(dev, struct sdioe_dev, dev);
    kfree(sdev);
}

static struct sdioe_dev *sdioe_device_create(struct sdio_dev *fdev)
{
    struct sdio_slot *slot = fdev->priv->slot;
    struct sdioe_dev *sdev;

    sdev = kzalloc(sizeof(struct sdioe_dev), GFP_KERNEL);
    if (sdev) {
        snprintf(sdev->dev.bus_id, sizeof(sdev->dev.bus_id),
                 "%s.%d", slot->name, fdev->function);
        sdev->dev.parent  = NULL;
        sdev->dev.bus     = &sdioe_bus_type;
        sdev->dev.release = sdioe_device_release;
    }
    return sdev;
}

int sdio_os_dev_add(struct sdio_dev *fdev)
{
    struct sdioe_dev *sdev;
    int err = -ENOMEM;

    sdev = sdioe_device_create(fdev);
    if (sdev) {
        err = device_register(&sdev->dev);
        if (err < 0) {
            kfree(sdev);
        } else {
            fdev->os_device = &sdev->dev;
        }
    }
    return err;
}

void sdio_os_dev_del(struct sdio_dev *fdev)
{
    if (fdev->os_device) {
        device_unregister(fdev->os_device);
        fdev->os_device = NULL;
    }
}

static int __init sdio_init(void)
{
    int err;

    err = bus_register(&sdioe_bus_type);
    if (err) {
        return err;
    }

    sdio_core_init();
    return 0;
}
module_init(sdio_init);

static void __exit sdio_exit(void)
{
    bus_unregister(&sdioe_bus_type);
}
module_exit(sdio_exit);

MODULE_DESCRIPTION("Embedded SDIO device driver");
MODULE_AUTHOR("Cambridge Silicon Radio Ltd.");
MODULE_LICENSE("GPL and additional rights");
