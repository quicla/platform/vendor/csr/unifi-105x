/*
 * Linux PXA27x MMC/SD controller driver.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/spinlock.h>
#include <linux/dma-mapping.h>
#include <linux/platform_device.h>
#include <linux/kernel-compat.h>

#include <asm/arch/pxa-regs.h>
#include <asm/io.h>
#include <asm/dma.h>
#include <asm/delay.h>

#include "slot_pxa27x.h"
#include "slot_pxa27x_lx.h"

struct pxa_sdio_controller {
    struct device *dev;

    spinlock_t lock;
    struct sdio_cmd *current_cmd;

    uint32_t mmc_i_mask;

    int irq;
    int dma;
    enum dma_data_direction dma_dir;
    dma_addr_t dma_addr;
};

static void pxa_sdio_hw_enable_int(struct pxa_sdio_controller *sdioc, uint32_t ints)
{
    unsigned long flags;

    spin_lock_irqsave(&sdioc->lock, flags);

    sdioc->mmc_i_mask &= ~ints;
    MMC_I_MASK = sdioc->mmc_i_mask;

    spin_unlock_irqrestore(&sdioc->lock, flags);
}

static void pxa_sdio_hw_disable_int(struct pxa_sdio_controller *sdioc, uint32_t ints)
{
    unsigned long flags;

    spin_lock_irqsave(&sdioc->lock, flags);

    sdioc->mmc_i_mask |= ints;
    MMC_I_MASK = sdioc->mmc_i_mask;

    spin_unlock_irqrestore(&sdioc->lock, flags);
}

static void pxa_sdio_hw_start_clock(struct pxa_sdio_controller *sdioc)
{
    MMC_STRPCL = START_CLOCK;
}


static void pxa_sdio_hw_stop_clock(struct pxa_sdio_controller *sdioc)
{
    unsigned timeout = 10000; /* 10 ms */

    MMC_STRPCL = STOP_CLOCK;

    /* Wait for clock to stop. */
    do { 
        uint32_t stat = MMC_STAT;
        if (!(stat & STAT_CLK_EN)) {
            break;
        }
        udelay(1);
    } while (--timeout);

    if (timeout == 0) {
        dev_err(sdioc->dev, "unable to stop MMCLK\n");
    }
}

static int pxa_sdio_hw_set_clock_rate(struct pxa_sdio_controller *sdioc, int clk)
{
    uint32_t clk_rate;

    for (clk_rate = 0; clk_rate <= 6; clk_rate++) {
        if (clk >= PXA27X_MMC_MMCLK_BASE_FREQ / (1 << clk_rate)) {
            break;
        }
    }

    MMC_CLKRT = clk_rate;
    pxa_sdio_hw_start_clock(sdioc);

    return PXA27X_MMC_MMCLK_BASE_FREQ / (1 << clk_rate);
}

static void pxa_sdio_hw_start(struct pxa_sdio_controller *sdioc)
{
    MMC_SPI = 0;
    MMC_RESTO = 64;
    /*
     * RDTO * 13128 ns is the maximum time between the end bit of the
     * response and the start bit of the data.  This isn't specified
     * by the SDIO standard so use the maximum possible (~ 80ms).
     */
    MMC_RDTO = RDTO_MAX;

    sdioc->mmc_i_mask = MMC_I_MASK_ALL;
    MMC_I_MASK = sdioc->mmc_i_mask;

    pxa_set_cken(CKEN12_MMC, 1);
}

static void pxa_sdio_hw_stop(struct pxa_sdio_controller *sdioc)
{
    MMC_I_MASK = MMC_I_MASK_ALL;

    pxa_sdio_hw_stop_clock(sdioc);
    pxa_set_cken(CKEN12_MMC, 0);
}

static uint32_t pxa_sdio_hw_response_r1(struct pxa_sdio_controller *sdioc)
{
    uint32_t r0, r1, r2;
    r0 = MMC_RES;
    r1 = MMC_RES;
    r2 = MMC_RES;
    return ((r0 & 0xff) << 24) | (r1 << 8) | (r2 >> 8);
}

static void pxa_sdio_cmd_complete(struct sdio_slot *slot)
{
    struct pxa_sdio_controller *sdioc = slot->drv_data;
    struct sdio_cmd *cmd = sdioc->current_cmd;
    uint32_t stat;

    pxa_sdio_hw_disable_int(sdioc, RES_ERR | DAT_ERR | END_CMD_RES
                           | DATA_TRAN_DONE | PRG_DONE);

    /* Check status for error bits. */
    stat = MMC_STAT;
    if (stat & STAT_TIME_OUT_RESPONSE) {
        cmd->status = SDD_CMD_ERR_CMD_TIMEOUT;
    } else if (stat & STAT_RES_CRC_ERR) {
        cmd->status = SDD_CMD_ERR_CMD_CRC;
    } else if (stat & (STAT_CRC_READ_ERROR | STAT_CRC_WRITE_ERROR)) {
        cmd->status = SDD_CMD_ERR_DAT_CRC;
    } else if (stat & STAT_READ_TIME_OUT) {
        cmd->status = SDD_CMD_ERR_DAT_TIMEOUT;
    } else {
        cmd->status = SDD_CMD_OK;
    }

    /* Read response if it's valid. */
    if (!(cmd->status & SDD_CMD_ERR_CMD)) {
        switch (cmd->flags & SDD_CMD_FLAG_RESP_MASK) {
        case SDD_CMD_FLAG_RESP_NONE:
            break;
        case SDD_CMD_FLAG_RESP_R1:
        case SDD_CMD_FLAG_RESP_R1B:
        case SDD_CMD_FLAG_RESP_R4:
        case SDD_CMD_FLAG_RESP_R5:
        case SDD_CMD_FLAG_RESP_R5B:
        case SDD_CMD_FLAG_RESP_R6:
            cmd->sdio.response.r1 = pxa_sdio_hw_response_r1(sdioc);
            break;
        default:
            dev_err(sdioc->dev, "response format not supported\n");
        }
    }

    if (cmd->data) {
        DCSR(sdioc->dma) = 0;
        dma_unmap_single(NULL, sdioc->dma_addr, cmd->len, sdioc->dma_dir);
    }

    sdio_cmd_complete(slot, cmd);
}

irqreturn_t pxa_sdio_int_handler(int irq, void *dev_id)
{
    struct sdio_slot *slot = dev_id;
    struct pxa_sdio_controller *sdioc = slot->drv_data;
    struct sdio_cmd *cmd = sdioc->current_cmd;
    uint32_t i_req;

    i_req = MMC_I_REG & ~sdioc->mmc_i_mask;

    /*
     * Mask any recieved interrupts as they can only be cleared by
     * starting a new command.
     */
    pxa_sdio_hw_disable_int(sdioc, i_req);

    if (i_req & SDIO_INT) {
        sdio_interrupt(slot);
    }

    if (i_req & (RES_ERR | DAT_ERR)) {
        pxa_sdio_cmd_complete(slot);
        return IRQ_HANDLED;
    }

    /*
     * Command has completed, read the response and notify the SDIO
     * layer unless we're waiting for a data transfer to complete.
     */
    if (i_req & END_CMD_RES) {
        if (!cmd->data) {
            pxa_sdio_cmd_complete(slot);
        } else {
            if (sdioc->dma_dir == DMA_TO_DEVICE) {
                /*
                 * When Tx'ing, start the DMA now that the response
                 * has been received (PXA270 Errata 91).
                 *
                 * Then wait for DATA_TRAN_DONE (not PRG_DONE, as
                 * PRG_DONE appears to not be cleared unless the card
                 * is in the busy state -- the PXA270 datasheet is not
                 * clear on when PRG_DONE is cleared).
                 */
                DCSR(sdioc->dma) = DCSR_NODESC | DCSR_RUN;
                pxa_sdio_hw_enable_int(sdioc, DATA_TRAN_DONE);
            } else {
                /*
                 * When Rx'ing, wait for the DMA complete interrupt to
                 * ensure that both the transfer from the card has
                 * completed and the data is written to memory.
                 */
            }
        }
    }

    if (i_req & DATA_TRAN_DONE) {
        /*
         * If an R1b or R5b (busy) response is expected and the card
         * is still busy, wait for the PRG_DONE interrupt.
         */
        if (((cmd->flags & SDD_CMD_FLAG_RESP_MASK) == SDD_CMD_FLAG_RESP_R5B ||
             (cmd->flags & SDD_CMD_FLAG_RESP_MASK) == SDD_CMD_FLAG_RESP_R1B)
            && !(MMC_STAT & STAT_PRG_DONE)) {
            pxa_sdio_hw_enable_int(sdioc, PRG_DONE);
        } else {
            pxa_sdio_cmd_complete(slot);
        }
    }

    if (i_req & PRG_DONE) {
        pxa_sdio_cmd_complete(slot);
    }

    return IRQ_HANDLED;
}

static void pxa_sdio_dma_int_handler(int dma, void *devid, struct pt_regs *regs)
{
    struct sdio_slot *slot = devid;
    struct pxa_sdio_controller *sdioc = slot->drv_data;

    DCSR(dma) = DCSR_ENDINTR;

    if (sdioc->dma_dir == DMA_FROM_DEVICE) {
        pxa_sdio_cmd_complete(slot);
    } else {
        /* Swap a partially filled TxD FIFO. */
        if (sdioc->current_cmd->len % PXA27X_MMC_FIFO_SIZE) {
            MMC_PRTBUF = BUF_PART_FULL;
        }
    }
}


static int pxa_set_bus_freq(struct sdio_slot *slot, unsigned clk)
{
    struct pxa_sdio_controller *sdioc = slot->drv_data;
    
    if (clk == SDD_BUS_FREQ_OFF) {
        pxa_sdio_hw_stop_clock(sdioc);
        return 0;
    } else if (clk == SDD_BUS_FREQ_IDLE) {
        /*
         * Reduce MMCLK instead of stopping it as the PXA27x MMC
         * controller requires MMCLK to detect SDIO interrupts.
         */
        clk = 400000;
    }
    return pxa_sdio_hw_set_clock_rate(sdioc, clk);
}

static int pxa_sdio_start_cmd(struct sdio_slot *slot, struct sdio_cmd *cmd)
{
    struct pxa_sdio_controller *sdioc = slot->drv_data;
    uint32_t nob, blklen, cmdat = CMDAT_SDIO_INT_EN;

    sdioc->current_cmd = cmd;

    /* The first command (which will always be a CMD0) must be
     * preceeded by at least 64 clocks. */
    if (cmd->sdio.cmd == 0) {
        cmdat |= CMDAT_INIT;
    }

    if (slot->bus_width == 4) {
        cmdat |= CMDAT_SD_4DAT;
    }

    switch (cmd->flags & SDD_CMD_FLAG_RESP_MASK) {
      case SDD_CMD_FLAG_RESP_NONE:
        cmdat |= CMDAT_RESP_NONE;
        break;
      case SDD_CMD_FLAG_RESP_R1B:
      case SDD_CMD_FLAG_RESP_R5B:
        cmdat |= CMDAT_BUSY;
        /* fall through */
      case SDD_CMD_FLAG_RESP_R1:
      case SDD_CMD_FLAG_RESP_R5:
      case SDD_CMD_FLAG_RESP_R6:
        cmdat |= CMDAT_RESP_SHORT;
        break;
      case SDD_CMD_FLAG_RESP_R2:
        cmdat |= CMDAT_RESP_R2;
        break;
      case SDD_CMD_FLAG_RESP_R3:
      case SDD_CMD_FLAG_RESP_R4:
        cmdat |= CMDAT_RESP_R3;
        break;
    }

    if (cmd->flags & SDD_CMD_FLAG_ABORT) {
        cmdat |= CMDAT_STOP_TRAN;
    }

    if (cmd->data) {
        cmdat |= CMDAT_DMAEN | CMDAT_DATAEN;
        cmdat |= (cmd->flags & SDD_CMD_FLAG_READ) ? 0 : CMDAT_WRITE;

        if (cmd->len < cmd->owner->blocksize) {
            blklen = cmd->len;
            nob = 1;
        } else {
            blklen = cmd->owner->blocksize;
            nob = cmd->len / blklen;
        }

        sdioc->dma_dir = (cmd->flags & SDD_CMD_FLAG_READ) ? DMA_FROM_DEVICE : DMA_TO_DEVICE;
        sdioc->dma_addr = dma_map_single(NULL, cmd->data, cmd->len, sdioc->dma_dir);

        DCSR(sdioc->dma) = DCSR_NODESC;
        if (sdioc->dma_dir == DMA_FROM_DEVICE) {
            DSADR(sdioc->dma) = __PREG(MMC_RXFIFO);
            DTADR(sdioc->dma) = sdioc->dma_addr;
            DCMD(sdioc->dma)  = (DCMD_INCTRGADDR | DCMD_FLOWSRC | DCMD_WIDTH1 |
                                 DCMD_BURST32 | DCMD_ENDIRQEN | (cmd->len & DCMD_LENGTH));
            /* Start the Rx DMA. */
            DCSR(sdioc->dma) = DCSR_NODESC | DCSR_RUN;
        } else {
            DSADR(sdioc->dma) = sdioc->dma_addr;
            DTADR(sdioc->dma) = __PREG(MMC_TXFIFO);
            DCMD(sdioc->dma)  = (DCMD_INCSRCADDR | DCMD_FLOWTRG | DCMD_WIDTH1 |
                                 DCMD_BURST32 | DCMD_ENDIRQEN | (cmd->len & DCMD_LENGTH));
            /*
             * Starting the Tx DMA here before the response has been
             * received results in incorrect data being Tx'd (PXA270
             * Errata 91).
             */
        }
    } else {
        nob = blklen = 0;
    }

    /* Start the command. */
    if (blklen) {
        MMC_BLKLEN = blklen;
    }
    if (nob) {
        MMC_NOB = nob;
    }
    MMC_CMD = cmd->sdio.cmd;
    MMC_ARGH = cmd->sdio.arg >> 16;
    MMC_ARGL = cmd->sdio.arg & 0xffff;
    MMC_CMDAT = cmdat;

    pxa_sdio_hw_enable_int(sdioc, RES_ERR | DAT_ERR | END_CMD_RES);

    return 0;
}

static int pxa_sdio_card_present(struct sdio_slot *slot)
{
    struct pxa_sdio_controller *sdioc = slot->drv_data;
    struct pxa27x_sdio_plat_data *pdata = sdioc->dev->platform_data;
    
    return pdata->card_present(sdioc->dev);
}

static int pxa_sdio_card_power(struct sdio_slot *slot, enum sdio_power power)
{
    struct pxa_sdio_controller *sdioc = slot->drv_data;
    struct pxa27x_sdio_plat_data *pdata = sdioc->dev->platform_data;
    
    return pdata->card_power(sdioc->dev, power);
}

static void pxa_sdio_enable_card_int(struct sdio_slot *slot)
{
    struct pxa_sdio_controller *sdioc = slot->drv_data;

    pxa_sdio_hw_enable_int(sdioc, SDIO_INT);
}

static void pxa_sdio_disable_card_int(struct sdio_slot *slot)
{
    struct pxa_sdio_controller *sdioc = slot->drv_data;

    pxa_sdio_hw_disable_int(sdioc, SDIO_INT);
}

static int pxa_sdio_probe(struct platform_device *dev)
{
    struct sdio_slot *slot;
    struct pxa_sdio_controller *sdioc;
    int ret;

    slot = sdio_slot_alloc(sizeof(struct pxa_sdio_controller));
    if (!slot) {
        return -ENOMEM;
    }
    platform_set_drvdata(dev, slot);

    strcpy(slot->name, dev->dev.bus_id);
    slot->type             = SDD_SLOT_TYPE_SD;
    slot->set_bus_freq     = pxa_set_bus_freq;
    slot->start_cmd        = pxa_sdio_start_cmd;
    slot->card_present     = pxa_sdio_card_present;
    slot->card_power       = pxa_sdio_card_power;
    slot->enable_card_int  = pxa_sdio_enable_card_int;
    slot->disable_card_int = pxa_sdio_disable_card_int;

    slot->caps.max_bus_width = 4;

    sdioc = slot->drv_data;

    sdioc->dev = &dev->dev;
    spin_lock_init(&sdioc->lock);

    pxa_sdio_hw_start(sdioc);

    sdioc->dma = pxa_request_dma(slot->name, DMA_PRIO_LOW, pxa_sdio_dma_int_handler, slot);
    if (sdioc->dma < 0) {
        ret = -EBUSY;
        goto error;
    }
    DCSR(sdioc->dma) = 0;
    DRCMRRXMMC = (sdioc->dma & DRCMR_CHLNUM) | DRCMR_MAPVLD;
    DRCMRTXMMC = (sdioc->dma & DRCMR_CHLNUM) | DRCMR_MAPVLD;
    DALGN |= 1 << sdioc->dma; /* FIXME: this should be set by the kernel. */

    ret = request_irq(IRQ_MMC, pxa_sdio_int_handler, 0, slot->name, slot);
    if (ret) {
        goto error;
    }
    sdioc->irq = IRQ_MMC;

    ret = sdio_slot_register(slot);
    if (ret) {
        goto error;
    }

    return 0;

  error:
    pxa_sdio_hw_stop(sdioc);
    if (sdioc->dma >= 0) {
        pxa_free_dma(sdioc->dma);
    }
    if (sdioc->irq) {
        free_irq(sdioc->irq, slot);
    }
    sdio_slot_free(slot);
    return ret;
}

static int pxa_sdio_remove(struct platform_device *dev)
{
    struct sdio_slot *slot = platform_get_drvdata(dev);
    struct pxa_sdio_controller *sdioc = slot->drv_data;

    sdio_slot_unregister(slot);

    pxa_sdio_hw_stop(sdioc);

    free_irq(sdioc->irq, slot);
    pxa_free_dma(sdioc->dma);

    sdio_slot_free(slot);

    return 0;
}

static int pxa_sdio_suspend(struct platform_device *dev, pm_message_t state)
{
    struct sdio_slot *slot = platform_get_drvdata(dev);
    struct pxa_sdio_controller *sdioc = slot->drv_data;

    /* Pass the event to the SDIO driver. */
    sdio_suspend(slot);
    /* Stop the SDIO clock, etc. */
    pxa_sdio_hw_stop(sdioc);

    return 0;
}

static int pxa_sdio_resume(struct platform_device *dev)
{
    struct sdio_slot *slot = platform_get_drvdata(dev);
    struct pxa_sdio_controller *sdioc = slot->drv_data;

    /* Re-initialise the SDIO H/W. */
    pxa_sdio_hw_start(sdioc);

    DRCMRRXMMC = (sdioc->dma & DRCMR_CHLNUM) | DRCMR_MAPVLD;
    DRCMRTXMMC = (sdioc->dma & DRCMR_CHLNUM) | DRCMR_MAPVLD;
    DALGN |= 1 << sdioc->dma; /* FIXME: this should be set by the kernel. */

    /* Pass the event to the SDIO driver. */
    sdio_resume(slot);
    return 0;
}


static struct platform_driver pxa27x_sdio_driver = {
    .probe  = pxa_sdio_probe,
    .remove = pxa_sdio_remove,
    .suspend= pxa_sdio_suspend,
    .resume = pxa_sdio_resume,
    .driver = {
        .name = "pxa27x-sdio",
    },
};

static int __init slot_pxa27x_init(void)
{
    return platform_driver_register(&pxa27x_sdio_driver);
}

static void __exit slot_pxa27x_exit(void)
{
    platform_driver_unregister(&pxa27x_sdio_driver);
}

module_init(slot_pxa27x_init);
module_exit(slot_pxa27x_exit);

MODULE_DESCRIPTION("PXA27x MMC/SD controller slot driver");
MODULE_AUTHOR("Cambridge Silicon Radio Ltd.");
MODULE_LICENSE("GPL and additional rights");
