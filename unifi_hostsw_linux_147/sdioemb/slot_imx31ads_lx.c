/*
 * Freescale i.MX27 ADS SDHC driver.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/platform_device.h>
//#include <asm/arch/mxc_pm.h>
#include "../drivers/mxc/mc13783_legacy/module/mc13783_power.h"
#include "sdio_layer.h"
#include "slot_imx31_lx.h"
extern unsigned int sdhc_get_card_det_status(struct device *dev);

#define SD_3_0V 7

static int imx31ads_sdio_card_present(struct platform_device *pdev)
{
//    return sdhc_get_card_det_status(&pdev->dev); 
    return pdev->id == 0 ? 1 : 0;
}

static int imx31ads_sdio_card_power(struct platform_device *pdev, enum sdio_power power)
{

    switch (power) {
    case SDIO_POWER_OFF:
        if (pdev->id == 0) {
            mc13783_power_conf_regu(REGU_VMMC1, false, true);
            mc13783_power_regu_en(REGU_VMMC1, false);
        } else {
            mc13783_power_conf_regu(REGU_VMMC2, false, true);
            mc13783_power_regu_en(REGU_VMMC2, false);

        }
        dev_dbg(&pdev->dev, "power off\n");
        break;
    case SDIO_POWER_3V3:
        if (pdev->id == 0) {
             mc13783_power_set_regu(REGU_VMMC1,
                                    SD_3_0V);
             mc13783_power_conf_regu(REGU_VMMC1, false, false);
             mc13783_power_regu_en(REGU_VMMC1, true);

        } else {
              mc13783_power_set_regu(REGU_VMMC2,
                                     SD_3_0V);
              mc13783_power_conf_regu(REGU_VMMC2, false, false);
              mc13783_power_regu_en(REGU_VMMC2, true);

        }
        dev_dbg(&pdev->dev, "power on\n");
        break;
    default:
        return -EINVAL;
    }

    return 0;
}

static void imx31ads_sdio_release(struct device *dev)
{
    /* No resources have been allocated. */
}

static struct resource sdhc1_resources[] = {
    {
        .start = MMC_SDHC1_BASE_ADDR,
        .end   = MMC_SDHC1_BASE_ADDR + SZ_16K -1,
        .flags = IORESOURCE_MEM,
    },
    {
        .start = INT_MMC_SDHC1,
        .end   = INT_MMC_SDHC1,
        .flags = IORESOURCE_IRQ,
    },
};

static struct imx_sdio_plat_data sdhc1_plat_data = {
    .max_bus_width = 4,
    .card_present  = imx31ads_sdio_card_present,
    .card_power    = imx31ads_sdio_card_power,
};

static struct resource sdhc2_resources[] = {
    {
        .start = MMC_SDHC2_BASE_ADDR,
        .end   = MMC_SDHC2_BASE_ADDR + SZ_16K -1,
        .flags = IORESOURCE_MEM,
    },
    {
        .start = INT_MMC_SDHC2,
        .end   = INT_MMC_SDHC2,
        .flags = IORESOURCE_IRQ,
    },
};

static struct imx_sdio_plat_data sdhc2_plat_data = {
    .max_bus_width = 4,
    .card_present  = imx31ads_sdio_card_present,
    .card_power    = imx31ads_sdio_card_power,
};

static struct platform_device imx31ads_sdio_devices[] = {
    {
        .name          = "imx31-sdio",
        .id            = 0,
        .num_resources = ARRAY_SIZE(sdhc1_resources),
        .resource      = sdhc1_resources,
        .dev           = {
            .platform_data = &sdhc1_plat_data,
            .release       = imx31ads_sdio_release,
        },
    },
    {
        .name          = "imx31-sdio",
        .id            = 1,
        .num_resources = ARRAY_SIZE(sdhc2_resources),
        .resource      = sdhc2_resources,
        .dev           = {
            .platform_data = &sdhc2_plat_data,
            .release       = imx31ads_sdio_release,
        },
    },
};

static int __init slot_imx31ads_init(void)
{
    int i, ret;

    //sdhc_init_card_det(0);
    for (i = 0; i < ARRAY_SIZE(imx31ads_sdio_devices); i++) {
        ret = platform_device_register(&imx31ads_sdio_devices[i]);
        if (ret) {
            while (--i >= 0) {
                platform_device_unregister(&imx31ads_sdio_devices[i]);
            }
            break;
        }
    }
    return ret;
}

static void __exit slot_imx31ads_exit(void)
{
    int i;

    for (i = 0; i < ARRAY_SIZE(imx31ads_sdio_devices); i++) {
        platform_device_unregister(&imx31ads_sdio_devices[i]);
    }
}

module_init(slot_imx31ads_init);
module_exit(slot_imx31ads_exit);

MODULE_DESCRIPTION("Freescale i.MX31 ADS SDIO slot driver");
MODULE_AUTHOR("Cambridge Silicon Radio Ltd.");
MODULE_LICENSE("GPL and additional rights");
