/*
 * Linux sdio spi private data definitions.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#ifndef _SLOT_SPI_LX_H
#define _SLOT_SPI_LX_H

struct sdio_spi_plat_data {
    struct work_struct spi_work;
    struct sdio_slot *slot;
    int (*enable_int)(struct sdio_slot *);
    int (*disable_int)(struct sdio_slot *);
};

#endif /* #ifndef _SLOT_SPI_LX_H */
