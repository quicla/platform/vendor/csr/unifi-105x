/* 
 * Sharp Zaurus C1000/3000/3200 SDIO device.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/delay.h>

#include <asm/arch/hardware.h>
#include <asm/arch/pxa-regs.h>

#include <asm/hardware/scoop.h>
#include <asm/arch/spitz.h>

#define SCP_CF_POWER SPITZ_SCP_CF_POWER
#define GPIO_nSD_DETECT SPITZ_GPIO_nSD_DETECT

#include "slot_pxa27x_lx.h"

static int sharp_cxx00_sdio_card_present(struct device *dev)
{
    return !(GPLR(GPIO_nSD_DETECT) & GPIO_bit(GPIO_nSD_DETECT));
}

static int sharp_cxx00_sdio_card_power(struct device *dev, enum sdio_power power)
{
    unsigned short cpr;

    if (power != SDIO_POWER_OFF) {
        set_scoop_gpio(&spitzscoop_device.dev, SPITZ_SCP_CF_POWER);

        cpr = read_scoop_reg(&spitzscoop_device.dev, SCOOP_CPR);
        if( ( cpr & (0x0004| 0x0002) ) == 0 )
        {
            msleep_interruptible(5);
        }
        cpr |= 0x0004;
        write_scoop_reg(&spitzscoop_device.dev, SCOOP_CPR, cpr );
    }
    else {
        cpr = read_scoop_reg(&spitzscoop_device.dev, SCOOP_CPR);
        cpr &= ~(0x004);
        write_scoop_reg(&spitzscoop_device.dev, SCOOP_CPR, cpr );

        msleep_interruptible(5);

        if (!(cpr & 0x2))
        {
            reset_scoop_gpio(&spitzscoop_device.dev, SPITZ_SCP_CF_POWER);
        }
    }

    return 0;
}

static struct pxa27x_sdio_plat_data sharp_cxx00_sdio_plat_data = {
    .card_present = sharp_cxx00_sdio_card_present,
    .card_power   = sharp_cxx00_sdio_card_power,
};

static struct platform_device sharp_cxx00_sdio_devices[] = {
    {
        .name          = "pxa27x-sdio",
        .id            = 0,
        .num_resources = 0,
        .dev           = {
            .platform_data = &sharp_cxx00_sdio_plat_data,
        },
    },
};

static int __init slot_sharp_cxx00_init(void)
{
    int i;

    /* Setup SDIO card detect GPIO */
    pxa_gpio_mode(GPIO_nSD_DETECT | GPIO_IN); /* inout */

    /*
     *  Make sure GPIOs are configured for SDIO operation.
     *  See PXA27x developers manual pages 24-5/6/7/8
     *  MMC CLK  GPIO  32 alternate function 2
     *  MMC DAT0 GPIO  92 alternate function 1
     *  MMC DAT1 GPIO 109    - ditto -
     *  MMC DAT2 GPIO 110    - ditto -
     *  MMC DAT3 GPIO 111    - ditto -
     *  MMC CMD  GPIO 112    - ditto -
     */
    pxa_gpio_mode(32 | GPIO_ALT_FN_2_OUT);
    pxa_gpio_mode(92 | GPIO_ALT_FN_1_OUT);
    pxa_gpio_mode(109 | GPIO_ALT_FN_1_OUT);
    pxa_gpio_mode(110 | GPIO_ALT_FN_1_OUT);
    pxa_gpio_mode(111 | GPIO_ALT_FN_1_OUT);
    pxa_gpio_mode(112 | GPIO_ALT_FN_1_OUT);

    for (i = 0; i < ARRAY_SIZE(sharp_cxx00_sdio_devices); i++) {
        platform_device_register(&sharp_cxx00_sdio_devices[i]);
    }

    return 0;
}

module_init(slot_sharp_cxx00_init);

MODULE_DESCRIPTION("Sharp Zaurus C1000/3000/32000 SDIO slot device");
MODULE_AUTHOR("Cambridge Silicon Radio Ltd.");
MODULE_LICENSE("GPL and additional rights");
