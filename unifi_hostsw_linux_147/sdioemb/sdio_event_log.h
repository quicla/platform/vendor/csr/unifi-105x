/*
 * Manage a log of the most recent SDIO events (commands and
 * interrupts etc.).
 *
 * Enable the log by #define'ing SDD_DEBUG_EVENT_LOG_LEN to the
 * maximum number of events to keep in the log.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */

#ifndef _SDIO_EVENT_LOG_H
#define _SDIO_EVENT_LOG_H

#ifdef __linux
#  define SDD_DEBUG_HAVE_EVENT_LOG
#endif

#ifndef SDD_DEBUG_EVENT_LOG_LEN
#  define SDD_DEBUG_EVENT_LOG_LEN 0
#endif

#ifndef SDD_DEBUG_DATA_BUFFER_LEN
#  define SDD_DEBUG_DATA_BUFFER_LEN 0
#endif

#if SDD_DEBUG_EVENT_LOG_LEN > 0 && defined(SDD_DEBUG_HAVE_EVENT_LOG)

#define SDD_DEBUG_EVENT_LOG

enum sdio_event_type {
    SDD_EVENT_CMD, SDD_EVENT_CARD_INT,
    SDD_EVENT_INT_UNMASKED, SDD_EVENT_INT_MASKED,
    SDD_EVENT_POWER_ON, SDD_EVENT_POWER_OFF, SDD_EVENT_RESET,
    SDD_EVENT_CLOCK_FREQ,
};

struct sdio_event_entry {
    unsigned seq_num;
    unsigned long time_ms;
    enum sdio_event_type type;
    union {
        struct {
            unsigned flags;
            union {
                struct sdio_cmd_resp sdio;
                struct cspi_cmd_resp cspi;
            };
            int status;
#if SDD_DEBUG_DATA_BUFFER_LEN > 0
            struct {
                unsigned length;
                unsigned start;
                uint8_t *buffer;
                struct sdio_event_entry *next;
            } data;
#endif
        } cmd;
        unsigned functions; /*< bit mask of relevant functions */
        int val;
    };
};

/* A ring buffer of SDIO events. */
struct sdio_event_log {
    struct sdio_event_entry *entries;
    unsigned head;
    unsigned tail;
    unsigned seq_num;
    os_spinlock_t lock;

#if SDD_DEBUG_DATA_BUFFER_LEN > 0
    /*
     * So that we don't display old data but can limit ourselves to
     * the last n octets of interesting data I play some clever tricks
     * here.  I store the sdio_cmd_event's that refer to data in a
     * linked list and also store the length of data blocks in this
     * list.  When I add a new block to the tail of this list that
     * increases the data size past the size of sdio_data_buffer I
     * chop things off the head of the list until the size is OK
     * again.
     */
    uint8_t *data_buffer;
    unsigned data_start;
    unsigned data_total;
    struct sdio_event_entry *data_head;
    struct sdio_event_entry *data_tail;
#endif
};

void sdio_event_log_init(struct sdio_slot *slot);
void sdio_event_log_deinit(struct sdio_slot *slot);
void sdio_event_log(struct sdio_slot *slot, enum sdio_event_type type, ...);
void sdio_event_log_set_response(struct sdio_slot *slot, const struct sdio_cmd *cmd);

struct sdio_event_entry *sdio_event_log_get_entry(struct sdio_slot *slot, unsigned n);
uint8_t sdio_event_log_get_data(struct sdio_event_entry *e, unsigned i);

void sdio_os_event_log_init(struct sdio_slot *slot);
void sdio_os_event_log_deinit(struct sdio_slot *slot);

#else

#undef SDD_DEBUG_EVENT_LOG

#define sdio_event_log_init(s)
#define sdio_event_log_deinit(s)
#define sdio_event_log(s, t, ...)
#define sdio_event_log_set_response(s, c)

#endif

#endif /* #ifndef _SDIO_EVENT_LOG_H */
