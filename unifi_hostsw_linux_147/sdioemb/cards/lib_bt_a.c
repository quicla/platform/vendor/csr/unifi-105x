/*
 * Core BT Type-A operations.
 *
 * Copyright (C) 2006-2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include <oska/all.h>

#include <sdioemb/sdio.h>
#include <sdioemb/sdio_api.h>
#include <sdioemb/sdio_cis.h>
#include <sdioemb/sdio_bt_a.h>

#define BC_FIRMWARE_START_TIMEOUT_MS 100
#define SDIO_CSR_TO_HOST_SCRATCH0_INITED 0x1

static void bt_warn(struct sdio_bt_a_dev *bt, const char *format, ...)
{
    va_list args;

    va_start(args, format);
    os_vprint(OS_PRINT_WARNING, "", bt->name, format, args);
    va_end(args);
}

static void bt_err(struct sdio_bt_a_dev *bt, const char *format, ...)
{
    va_list args;

    va_start(args, format);
    os_vprint(OS_PRINT_ERROR, "", bt->name, format, args);
    va_end(args);
}

/**
 * Read CIS to determine if read acknowledges are required.
 *
 * @param bt the BT device.
 *
 * @return true if read acknowledges are required; false otherwise.
 */
static int needs_read_ack(struct sdio_bt_a_dev *bt)
{
    uint8_t tpl[3];
    int err;

    err = sdio_cis_get_tuple(bt->fdev, CISTPL_SDIO_STD, tpl, sizeof(tpl));
    if (err) {
        /* If the tuple doesn't exist and this is a CSR chip, then
           read acks are not required. */
        if (err == -ENXIO && bt->fdev->vendor_id == SDIO_MANF_ID_CSR) {
            return 0;
        }
        return 1;
    }

    return tpl[2] == 0x00;
}

/**
 * Disable read acknowledges if supported by the hardware.
 *
 * @param bt the BT device.
 */
static void disable_read_acks(struct sdio_bt_a_dev *bt)
{
    int err;

    if (bt->needs_read_ack) {
        return;
    }

    err = sdio_write8(bt->fdev, SDIO_BT_A_RETRY_CTRL, RTC_SET);
    if (err) {
        bt_warn(bt, "could not disable read acks (%d)", err);
        bt->needs_read_ack = 1;
    }
}

/**
 * Start a BT Type-A function.
 *
 * The function is enabled, read acknowledges are disabled (if
 * supported by the hardware) and finally interrupts are enabled.
 *
 * The sleep state is also set to AWAKE to ensure the software and
 * hardware deep sleep states are in sync.
 *
 * @param bt the BT device.
 */
void sdio_bt_a_start(struct sdio_bt_a_dev *bt)
{
    int timeout_ms = BC_FIRMWARE_START_TIMEOUT_MS;

    sdio_bt_a_set_sleep_state(bt, SLEEP_STATE_AWAKE);
    sdio_enable_function(bt->fdev);

    /*
     * For some hardware the I/O Ready bit is set before the device is
     * ready. In this case we wait for firmware to start by polling
     * bit 0 in TO_HOST_SCRATCH0.
     */
    if (bt->wait_for_firmware) {
        while (--timeout_ms) {
            uint8_t scratch0;
            sdio_f0_read8(bt->fdev, SDIO_CSR_TO_HOST_SCRATCH0, &scratch0);
            if (scratch0 == SDIO_CSR_TO_HOST_SCRATCH0_INITED) {
                break;
            }
            os_sleep_ms(1);
        }
        if (timeout_ms == 0) {
            bt_warn(bt, "timed out waiting for firmware to start");
        }
    }

    disable_read_acks(bt);
    sdio_write8(bt->fdev, SDIO_BT_A_INT_EN, EN_INTRD);
}

/**
 * Stop a BT Type-A function.
 *
 * The function is disabled.
 *
 * @param bt the BT device.
 */
void sdio_bt_a_stop(struct sdio_bt_a_dev *bt)
{
    sdio_disable_function(bt->fdev);
}

/**
 * Restart the function if a hardware reset occurred.
 *
 * If the hardware is reset by an internal watchdog (or similar), the
 * function must be reenabled prior to attempting any SDIO commands to
 * it.
 *
 * A watchdog reset is identified by the I/O Enable bit being set and
 * the Extended I/O Enable bit being clear.  In all uses of the
 * function we expect I/O Enable to be set so on the Extended I/O
 * Enable bit is checked.
 *
 * @param bt the BT device.
 *
 * @return 1 if a reset had occurred; 0 if a reset had not occurred;
 * -ve on error (-ENODEV, -EIO etc).
 */
int sdio_bt_a_check_for_reset(struct sdio_bt_a_dev *bt)
{
    uint8_t ext_io_en;
    int ret;

    ret = sdio_f0_read8(bt->fdev, SDIO_CSR_EXT_IO_EN, &ext_io_en);
    if (ret == 0 && !(ext_io_en & (1 << bt->fdev->function))) {
        bt_warn(bt, "watchdog reset detected");
        sdio_bt_a_start(bt);
        return 1;
    }
    return ret;
}

/**
 * Write a packet to the hardware.
 *
 * The entire packet is written in a single SDIO command.
 *
 * I/O errors cause the packet write to be retries, fatal errors (too
 * many retries) will be reported to the caller.
 *
 * @param bt     the BT device.
 * @param packet packet to write including the Type-A header.
 * @param len    total length of the packet.
 *
 * @return 0 on success; -ve on error.
 */
int sdio_bt_a_send(struct sdio_bt_a_dev *bt, const struct sdio_bt_a_pkt_info *pkt)
{
    int ret = 0;
    int retry;

    for (retry = 0; retry < bt->max_tx_retries; retry++) {
        ret = sdio_write(bt->fdev, SDIO_BT_A_TD, pkt->hdr, pkt->data_len + SDIO_BT_A_HEADER_LEN);
        if (ret == 0)
            break;

        bt_warn(bt, "write packet failed (%d), retrying", ret);
        sdio_write8(bt->fdev, SDIO_BT_A_TX_PKT_CTRL, PC_WRT);
    }

    if (retry >= bt->max_tx_retries) {
        bt_err(bt, "maximum number of write retries exceeded");
        sdio_bt_a_check_for_reset(bt);
    }

    return ret;
}

/**
 * Read a packet from the hardware.
 *
 * The 4 octet Type-A transport header and part of the payload data is
 * read.  The header is used to determine the total length of the
 * packet and the remaining data (if any) is read (using a read padded
 * to a whole number of blocks).
 *
 * If an I/O error occurs the packet read is retried.  Fatal errors
 * (no memory, too many retries) cause the packet to be silently
 * discarded.
 *
 * The read packet ready interrupt is cleared prior to reading (or
 * re-reading) a packet so the hardware will make the packet data
 * available.
 *
 * @param bt  the BT device.
 * @param pkt read packet information.
 *
 * @return 0 on success; -ve on error.
 */
static int rx_packet(struct sdio_bt_a_dev *bt, struct sdio_bt_a_pkt_info *pkt)
{
    int ret = 0;
    int retry = 0;
    int remaining_len = 0;

    pkt->data_len = 0;
    pkt->data = os_alloc(SDIO_BT_A_HEADER_LEN + SDIO_BT_A_MIN_READ);
    if (pkt->data == NULL) {
        bt_err(bt, "no memory for Rx packet");
        ret = -ENOMEM;
        goto done;
    }

    do {
        sdio_write8(bt->fdev, SDIO_BT_A_INTRD, CL_INTRD);

        /* Read header and part of the packet data. */
        ret = sdio_read(bt->fdev, SDIO_BT_A_RD, pkt->data,
                        SDIO_BT_A_HEADER_LEN + SDIO_BT_A_MIN_READ);
        if (ret < 0) {
            goto retry_read;
        }

        if (pkt->data_len == 0) {
            memcpy(pkt->hdr, pkt->data, SDIO_BT_A_HEADER_LEN);

            pkt->data_len = sdio_bt_a_packet_len(pkt->hdr) - SDIO_BT_A_HEADER_LEN;

            if (pkt->data_len <= 0 || pkt->data_len > SDIO_BT_A_PACKET_LEN_MAX) {
                bt_err(bt, "bad rx packet length (%d)", pkt->data_len);
                ret = -EINVAL;
                goto done;
            }

            if (pkt->data_len > SDIO_BT_A_MIN_READ) {
                remaining_len = pkt->data_len - SDIO_BT_A_MIN_READ;
                if (remaining_len > bt->fdev->blocksize) {
                    remaining_len = (remaining_len + bt->fdev->blocksize - 1)
                        / bt->fdev->blocksize * bt->fdev->blocksize;
                }
            }

            /* Allocate more memory if required and move the packet
               data to the start of the buffer */
            if (remaining_len + SDIO_BT_A_MIN_READ > SDIO_BT_A_MIN_READ) {
                int buf_size;
                void *new_data;

                buf_size = SDIO_BT_A_MIN_READ + remaining_len;

                new_data = os_alloc(buf_size);
                if (new_data == NULL) {
                    bt_err(bt, "no memory for Rx packet");
                    ret = -ENOMEM;
                    goto done;
                }
                memcpy(new_data, pkt->data + SDIO_BT_A_HEADER_LEN, SDIO_BT_A_MIN_READ);
                os_free(pkt->data);
                pkt->data = new_data;
            } else {
                memmove(pkt->data, pkt->data + SDIO_BT_A_HEADER_LEN, pkt->data_len);
            }
        }

        /* Read any remaining data. */
        if (remaining_len) {
            ret = sdio_read(bt->fdev, SDIO_BT_A_RD,
                            pkt->data + SDIO_BT_A_MIN_READ, remaining_len);
        }
        if (ret == 0)
            goto done;

      retry_read:
        sdio_write8(bt->fdev, SDIO_BT_A_RX_PKT_CTRL, PC_RRT);
    } while (++retry < bt->max_rx_retries);

  done:
    if (ret != -ENODEV) {
        /* Acknowledge read if the hardware requires it, or if an error
           occured (so the hardware discards the packet). */
        if (bt->needs_read_ack || ret < 0) {
            sdio_write8(bt->fdev, SDIO_BT_A_RX_PKT_CTRL, 0x00);
        }
        if (retry >= bt->max_rx_retries && ret != -ENODEV) {
            bt_err(bt, "maximum number of read retries exceeded");
            sdio_bt_a_check_for_reset(bt);
        }
    }
    if (ret < 0)
        os_free(pkt->data);
    return ret;
}

/**
 * Set the deep sleep state.
 *
 * The deep sleep state management is necessary as SDIO commands to
 * the BT Type-A function fail when the hardware is in deep sleep,
 * thus prior to reading or writing packets we must ensure the chip is
 * awake.
 *
 * The following state transitions are handled.
 *
 * AWAKE -> TORPID: In response to an idle timer expiring (or
 * similar), simply write TORPID to the state register to permit the
 * chip to enter deep sleep.
 *
 * TORPID -> DROWSY: In response to the host software wishing to write
 * a packet, set the state to DROWSY and wait for the state to be
 * AWAKE.
 *
 * DROWSY/TORPID -> AWAKE: An interrupt indicates that the chip is
 * awake, set the state to AWAKE and wake up any threads waiting for
 * AWAKE.
 *
 * @param bt    the BT device.
 * @param state state to transition to.
 */
void sdio_bt_a_set_sleep_state(struct sdio_bt_a_dev *bt, enum sdio_sleep_state state)
{
    int r;

    if (state == bt->sleep_state) {
        return;
    }

    /*
     * The awake interrupt is sourced from the 'core clock running'
     * signal, this is deasserted 1 slow clock (1 kHz) cycle after the
     * core clock is stopped.  If the DROWSY state is set within 1 ms
     * of the core clock being stopped then a spurious wake up
     * interrupt occurs.
     *
     * Setting AWAKE and then waiting 1 ms before setting DROWSY
     * ensures we don't set DROWSY during this unsafe window.
     */
    if (state == SLEEP_STATE_DROWSY) {
        sdio_f0_write8(bt->fdev, SDIO_CSR_SLEEP_STATE,
                       SDIO_CSR_SLEEP_STATE_FUNC(bt->fdev->function) | SLEEP_STATE_AWAKE);
        os_sleep_ms(1);
    }

    r = sdio_f0_write8(bt->fdev, SDIO_CSR_SLEEP_STATE,
                       SDIO_CSR_SLEEP_STATE_FUNC(bt->fdev->function) | state);
    if (r != 0) {
        return;
    }
    bt->sleep_state = state;

    bt->sleep_state_changed(bt);
}

/**
 * Handle an interrupt from the function.
 *
 * The chip will only raise an interrupts if it is awake and will not
 * enter deep sleep while an interrupt is asserted, so we first set
 * the sleep state to AWAKE.
 *
 * The sources of interrupt is then identified and handled.  The three
 * possible sources are:
 *
 *   1. Read packet ready.  The packet is read from the hardware and
 *      added to the received packet queue.
 *
 *   2. Awake.  This may only occur when the sleep state is DROWSY and
 *      no action other than setting the sleep state is required.
 *
 *   3. Generic interrupt.  The hardware provides a facility for the
 *      firmware to raise an interrupt (via the SDIO_HOST_INT
 *      register).  The current firmware makes no use of this so we
 *      just clear the interrupt.
 *
 * @param bt the BT device.
 */
void sdio_bt_a_handle_interrupt(struct sdio_bt_a_dev *bt)
{
    uint8_t rdint;
    int err;
    int wake_up_expected;
    struct sdio_bt_a_pkt_info pkt;

    wake_up_expected = bt->sleep_state == SLEEP_STATE_DROWSY;
    sdio_bt_a_set_sleep_state(bt, SLEEP_STATE_AWAKE);

    /* If a wake up isn't expected, the only interrupt source is
       'packet read ready' so skip reading the INTRD register. */
    if (wake_up_expected) {
        err = sdio_read8(bt->fdev, SDIO_BT_A_INTRD, &rdint);
        if (err) {
            if (err != -ENODEV && sdio_bt_a_check_for_reset(bt) == 1) {
                err = sdio_read8(bt->fdev, SDIO_BT_A_INTRD, &rdint);
            }
            if (err) {
                return;
            }
        }
        if (!(rdint & INTRD)) {
            return;
        }
    }

    err = rx_packet(bt, &pkt);
    if (!err) {
        bt->receive(bt, &pkt);
    }
}

void sdio_bt_a_init(struct sdio_bt_a_dev *bt, struct sdio_dev *fdev)
{
    bt->fdev              = fdev;
    bt->max_tx_retries    = 3;
    bt->max_rx_retries    = 3;
    bt->needs_read_ack    = needs_read_ack(bt);
    bt->wait_for_firmware = 0;
    bt->sleep_state       = SLEEP_STATE_TORPID; /* safe state to assume */
}
