/*
 * Linux USB SD Host Controller (USHC) slot driver.
 *
 * Copyright (C) 2008 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * References:
 *   [USHC] USB SD Host Controller specification (CS-118793-SP)
 */
#include <linux/module.h>
#include <linux/usb.h>
#include <linux/kernel.h>
#include <linux/usb.h>

#include <sdioemb/sdio.h>
#include <sdioemb/slot_api.h>

enum ushc_request {
    USHC_GET_CAPS  = 0x00,
    USHC_HOST_CTRL = 0x01,
    USHC_PWR_CTRL  = 0x02,
    USHC_CLK_FREQ  = 0x03,
    USHC_EXEC_CMD  = 0x04,
    USHC_READ_RESP = 0x05,
};

enum ushc_request_type {
    USHC_GET_CAPS_TYPE  = USB_DIR_IN  | USB_TYPE_VENDOR | USB_RECIP_DEVICE,
    USHC_HOST_CTRL_TYPE = USB_DIR_OUT | USB_TYPE_VENDOR | USB_RECIP_DEVICE,
    USHC_PWR_CTRL_TYPE  = USB_DIR_OUT | USB_TYPE_VENDOR | USB_RECIP_DEVICE,
    USHC_CLK_FREQ_TYPE  = USB_DIR_OUT | USB_TYPE_VENDOR | USB_RECIP_DEVICE,
    USHC_EXEC_CMD_TYPE  = USB_DIR_OUT | USB_TYPE_VENDOR | USB_RECIP_DEVICE,
    USHC_READ_RESP_TYPE = USB_DIR_IN  | USB_TYPE_VENDOR | USB_RECIP_DEVICE,
};

#define USHC_GET_CAPS_3V3      (1 << 8)
#define USHC_GET_CAPS_3V0      (1 << 9)
#define USHC_GET_CAPS_1V8      (1 << 10)
#define USHC_GET_CAPS_HIGH_SPD (1 << 16)

#define USHC_HOST_CTRL_4BIT     (1 << 1)
#define USHC_HOST_CTRL_HIGH_SPD (1 << 0)

#define USHC_PWR_CTRL_OFF 0x00
#define USHC_PWR_CTRL_3V3 0x01
#define USHC_PWR_CTRL_3V0 0x02
#define USHC_PWR_CTRL_1V8 0x03

#define USHC_READ_RESP_BUSY        (1 << 4)
#define USHC_READ_RESP_ERR_TIMEOUT (1 << 3)
#define USHC_READ_RESP_ERR_CRC     (1 << 2)
#define USHC_READ_RESP_ERR_DAT     (1 << 1)
#define USHC_READ_RESP_ERR_CMD     (1 << 0)
#define USHC_READ_RESP_ERR_MASK    0x0f

struct ushc_exec_cmd_req {
    u8     bRequestType;
    u8     bRequest;
    u8     cmd_idx;
    u8     reserved;
    __le16 block_size;
    __le16 wLength;
};

struct ushc_exec_cmd_data {
    __le32 arg;
};

struct ushc_read_resp_req {
    u8     bRequestType;
    u8     bRequest;
    u8     reserved[4];
    __le16 wLength;
};

struct ushc_read_resp_data {
    u8     status;
    u8     reserved[3];
    __le32 resp;
};

#define USHC_INT_STATUS_SDIO_INT     (1 << 1)
#define USHC_INT_STATUS_CARD_PRESENT (1 << 0)

struct ushc_int_data {
    u8 status;
    u8 reserved[3];
};

struct ushc_data {
    struct usb_device *usb_dev;

    struct urb *int_urb;
    struct ushc_int_data *int_data;

    struct urb *cmd_urb;
    struct ushc_exec_cmd_req *cmd_req;
    struct ushc_exec_cmd_data *cmd_data;

    struct urb *data_urb;

    struct urb *resp_urb;
    struct ushc_read_resp_req *resp_req;
    struct ushc_read_resp_data *resp_data;

    spinlock_t lock;
    struct sdio_cmd *current_cmd;
    u32 caps;
    u16 host_ctrl;
    int disconnected;
    int int_enabled;
};

static void data_callback(struct urb *urb);

static int ushc_hw_get_caps(struct ushc_data *ushc)
{
    int ret;

    ret = usb_control_msg(ushc->usb_dev, usb_rcvctrlpipe(ushc->usb_dev, 0),
                          USHC_GET_CAPS, USHC_GET_CAPS_TYPE,
                          0, 0, &ushc->caps, sizeof(ushc->caps), 100);
    if (ret < 0) {
        return ret;
    }

    ushc->caps = le32_to_cpu(ushc->caps);
    return 0;
}
    

static int ushc_hw_set_host_ctrl(struct ushc_data *ushc, u16 mask, u16 val)
{
    u16 host_ctrl;
    int ret;

    host_ctrl = (ushc->host_ctrl & ~mask) | val;
    ret = usb_control_msg(ushc->usb_dev, usb_sndctrlpipe(ushc->usb_dev, 0),
                          USHC_HOST_CTRL, USHC_HOST_CTRL_TYPE,
                          host_ctrl, 0, NULL, 0, 100);
    if (ret >= 0) {
        ushc->host_ctrl = host_ctrl;
    }
    return ret;
}

static void int_callback(struct urb *urb)
{
    struct sdio_slot *slot = urb->context;
    struct ushc_data *ushc = slot->drv_data;

    if (ushc->int_data->status & USHC_INT_STATUS_SDIO_INT
        && ushc->int_enabled) {
        sdio_interrupt(slot);
    }

    if (ushc->int_urb->status == 0) {
        usb_submit_urb(ushc->int_urb, GFP_ATOMIC);
    }
}

static void cmd_callback(struct urb *urb)
{
    struct sdio_slot *slot = urb->context;
    struct ushc_data *ushc = slot->drv_data;
    struct sdio_cmd *cmd = ushc->current_cmd;
    int ret;

    if (cmd->data) {
        int pipe;

        if (cmd->flags & SDD_CMD_FLAG_READ)
            pipe = usb_rcvbulkpipe(ushc->usb_dev, 6);
        else
            pipe = usb_sndbulkpipe(ushc->usb_dev, 2);
            
        usb_fill_bulk_urb(ushc->data_urb, ushc->usb_dev, pipe,
                          cmd->data, cmd->len, data_callback, slot);
        ret = usb_submit_urb(ushc->data_urb, GFP_ATOMIC);
        if (ret < 0) {
            usb_unlink_urb(ushc->resp_urb);
        }
    }
}

static void resp_callback(struct urb *urb)
{
    struct sdio_slot *slot = urb->context;
    struct ushc_data *ushc = slot->drv_data;
    struct sdio_cmd *cmd = ushc->current_cmd;
    int status;
    int ret;

    status = ushc->resp_data->status;

    if (urb->status != 0) {
        cmd->status = SDD_CMD_ERR_CMD_OTHER;
    } else if (status & USHC_READ_RESP_BUSY) {
        ret = usb_submit_urb(ushc->resp_urb, GFP_ATOMIC);
        if (ret < 0) {
            cmd->status = SDD_CMD_ERR_CMD_OTHER;
        } else {
            return;
        }
    } else if (status & USHC_READ_RESP_ERR_MASK) {
        if (status & USHC_READ_RESP_ERR_DAT) {
            cmd->status = SDD_CMD_ERR_DAT;
        } else {
            cmd->status = SDD_CMD_ERR_CMD;
        }
        if (status & USHC_READ_RESP_ERR_CRC) {
            cmd->status |= SDD_CMD_ERR_CRC;
        }
        if (status & USHC_READ_RESP_ERR_TIMEOUT) {
            cmd->status |= SDD_CMD_ERR_TIMEOUT;
        }
        usb_unlink_urb(ushc->data_urb);
    }

    if (cmd->status == SDD_CMD_IN_PROGRESS) {
        cmd->status = SDD_CMD_OK;
    }

    cmd->sdio.response.r1 = le32_to_cpu(ushc->resp_data->resp);

    sdio_cmd_complete(slot, cmd);
}

static void data_callback(struct urb *urb)
{
    struct sdio_slot *slot = urb->context;
    struct ushc_data *ushc = slot->drv_data;

    if (urb->status != 0) {
        usb_unlink_urb(ushc->resp_urb);
    }
}


static int ushc_start_cmd(struct sdio_slot *slot, struct sdio_cmd *cmd)
{
    struct ushc_data *ushc = slot->drv_data;
    struct urb *cmd_urb = ushc->cmd_urb;
    int ret;

    spin_lock(&ushc->lock);

    if (ushc->disconnected) {
        cmd->status = SDD_CMD_ERR_NO_CARD;
        sdio_cmd_complete(slot, cmd);
        ret = -ENODEV;
        goto out;
    }

    ushc->current_cmd = cmd;

    /* fill urb(s) */
    ushc->cmd_req->cmd_idx = cmd->sdio.cmd;
    ushc->cmd_req->block_size = cpu_to_le16(cmd->owner->blocksize);
    ushc->cmd_data->arg = cpu_to_le32(cmd->sdio.arg);
    printk("%x\n", ushc->cmd_data->arg);

    /* start cmd with ctrl (+ bulk urb) */
    ret = usb_submit_urb(cmd_urb, GFP_ATOMIC);
    if (ret < 0) {
        goto out;
    }

    ret = usb_submit_urb(ushc->resp_urb, GFP_ATOMIC);
    if (ret < 0) {
        usb_unlink_urb(cmd_urb);
        goto out;
    }

out:
    spin_unlock(&ushc->lock);
    return ret;
}

static int ushc_set_bus_freq(struct sdio_slot *slot, int clk)
{
    struct ushc_data *ushc = slot->drv_data;
    int ret;

    /* Hardware can't detect interrupts while the clock is off. */
    if (clk == SDD_BUS_FREQ_IDLE) {
        clk = 400000;
    }

    ret = ushc_hw_set_host_ctrl(ushc, USHC_HOST_CTRL_HIGH_SPD,
                                clk > SDIO_CLOCK_FREQ_NORMAL_SPD ? USHC_HOST_CTRL_HIGH_SPD : 0);
    if (ret < 0) {
        return ret;
    }

    /* FIXME: shouldn't sleep here! */
    ret = usb_control_msg(ushc->usb_dev, usb_sndctrlpipe(ushc->usb_dev, 0),
                          USHC_CLK_FREQ, USHC_CLK_FREQ_TYPE,
                          clk & 0xffff, (clk >> 16) & 0xffff, NULL, 0, 100);
    if (ret >= 0) {
        ret = clk;
    }
    return ret;
}

static int ushc_set_bus_width(struct sdio_slot *slot, int bus_width)
{
    struct ushc_data *ushc = slot->drv_data;

    return ushc_hw_set_host_ctrl(ushc, USHC_HOST_CTRL_4BIT,
                                 bus_width == 4 ? USHC_HOST_CTRL_4BIT : 0);
}

static int ushc_card_power(struct sdio_slot *slot, enum sdio_power power)
{
    struct ushc_data *ushc = slot->drv_data;
    u16 voltage;

    switch (power) {
    case SDIO_POWER_OFF:
        voltage = USHC_PWR_CTRL_OFF;
        break;
    case SDIO_POWER_3V3:
        if (!(ushc->caps & USHC_GET_CAPS_3V3)) {
            return -ENOTSUPP;
        }
        voltage = USHC_PWR_CTRL_3V3;
        break;
    default:
        return -EINVAL;
    }

    return usb_control_msg(ushc->usb_dev, usb_sndctrlpipe(ushc->usb_dev, 0),
                           USHC_PWR_CTRL, USHC_PWR_CTRL_TYPE,
                           voltage, 0, NULL, 0, 100);
}

static int ushc_card_present(struct sdio_slot *slot)
{
    struct ushc_data *ushc = slot->drv_data;

    return ushc->int_data->status & USHC_INT_STATUS_CARD_PRESENT;
}

static void ushc_enable_card_int(struct sdio_slot *slot)
{
    struct ushc_data *ushc = slot->drv_data;

    ushc->int_enabled = 1;
}

static void ushc_disable_card_int(struct sdio_slot *slot)
{
    struct ushc_data *ushc = slot->drv_data;

    ushc->int_enabled = 0;
}

static void ushc_clean_up(struct sdio_slot *slot)
{
    struct ushc_data *ushc = slot->drv_data;

    usb_free_urb(ushc->int_urb);
    usb_free_urb(ushc->cmd_urb);
    usb_free_urb(ushc->resp_urb);

    kfree(ushc->int_data);
    kfree(ushc->cmd_data);
    kfree(ushc->cmd_req);
    kfree(ushc->resp_data);
    kfree(ushc->resp_req);
}

static int ushc_probe(struct usb_interface *intf, const struct usb_device_id *id)
{
    struct usb_device *usb_dev = interface_to_usbdev(intf);
    struct sdio_slot *slot;
    struct ushc_data *ushc;
    int ret = -ENOMEM;

    slot = sdio_slot_alloc(sizeof(struct ushc_data));
    if (slot == NULL) {
        return -ENOMEM;
    }
    ushc = slot->drv_data;
    usb_set_intfdata(intf, slot);

    ushc->usb_dev = usb_dev;

    strlcpy(slot->name, usb_dev->dev.bus_id, sizeof(slot->name));
    slot->set_bus_freq     = ushc_set_bus_freq;
    slot->set_bus_width    = ushc_set_bus_width;
    slot->start_cmd        = ushc_start_cmd;
    slot->card_present     = ushc_card_present;
    slot->card_power       = ushc_card_power;
    slot->enable_card_int  = ushc_enable_card_int;
    slot->disable_card_int = ushc_disable_card_int;

    spin_lock_init(&ushc->lock);

    /* Read capabilities. */
    ret = ushc_hw_get_caps(ushc);
    if (ret < 0) {
        goto err;
    }
    slot->caps.max_bus_freq = (ushc->caps & USHC_GET_CAPS_HIGH_SPD)
        ? SDIO_CLOCK_FREQ_HIGH_SPD : SDIO_CLOCK_FREQ_NORMAL_SPD;
    slot->caps.max_bus_width = 4;

    ushc->int_urb = usb_alloc_urb(0, GFP_KERNEL);
    if (ushc->int_urb == NULL) {
        goto err;
    }
    ushc->int_data = kzalloc(sizeof(struct ushc_int_data), GFP_KERNEL);
    if (ushc->int_data == NULL) {
        goto err;
    }
    usb_fill_int_urb(ushc->int_urb, ushc->usb_dev,
                     usb_rcvintpipe(usb_dev,
                                    intf->cur_altsetting->endpoint[0].desc.bEndpointAddress),
                     ushc->int_data, sizeof(struct ushc_int_data),
                     int_callback, slot,
                     intf->cur_altsetting->endpoint[0].desc.bInterval);

    ushc->cmd_req = kzalloc(sizeof(struct ushc_exec_cmd_req), GFP_KERNEL);
    if (ushc->cmd_req == NULL) {
        goto err;
    }
    ushc->cmd_data = kzalloc(sizeof(struct ushc_exec_cmd_data), GFP_KERNEL);
    if (ushc->cmd_data == NULL) {
        goto err;
    }
    ushc->cmd_urb = usb_alloc_urb(0, GFP_KERNEL);
    if (ushc->cmd_urb == NULL) {
        goto err;
    }
    ushc->cmd_req->bRequestType = USHC_EXEC_CMD_TYPE;
    ushc->cmd_req->bRequest     = USHC_EXEC_CMD;
    ushc->cmd_req->wLength      = cpu_to_le16(sizeof(struct ushc_exec_cmd_data));
    usb_fill_control_urb(ushc->cmd_urb, ushc->usb_dev, usb_sndctrlpipe(usb_dev, 0),
                         (void *)ushc->cmd_req,
                         ushc->cmd_data, sizeof(struct ushc_exec_cmd_data),
                         cmd_callback, slot);

    ushc->resp_req = kzalloc(sizeof(struct ushc_read_resp_req), GFP_KERNEL);
    if (ushc->resp_req == NULL) {
        goto err;
    }
    ushc->resp_data = kzalloc(sizeof(struct ushc_read_resp_data), GFP_KERNEL);
    if (ushc->resp_data == NULL) {
        goto err;
    }
    ushc->resp_urb = usb_alloc_urb(0, GFP_KERNEL);
    if (ushc->cmd_urb == NULL) {
        goto err;
    }
    ushc->resp_req->bRequestType = USHC_READ_RESP_TYPE;
    ushc->resp_req->bRequest     = USHC_READ_RESP;
    ushc->resp_req->wLength      = cpu_to_le16(sizeof(struct ushc_read_resp_data));
    usb_fill_control_urb(ushc->resp_urb, ushc->usb_dev, usb_rcvctrlpipe(usb_dev, 0),
                         (void *)ushc->resp_req,
                         ushc->resp_data, sizeof(struct ushc_read_resp_data),
                         resp_callback, slot);

    ushc->data_urb = usb_alloc_urb(0, GFP_KERNEL);
    if (ushc->data_urb == NULL) {
        goto err;
    }

    ret = sdio_slot_register(slot);
    if (ret) {
        goto err;
    }

    ret = usb_submit_urb(ushc->int_urb, GFP_KERNEL);
    if (ret < 0) {
        sdio_slot_unregister(slot);
        goto err;
    }

    return 0;

  err:
    ushc_clean_up(slot);
    sdio_slot_free(slot);
    return ret;
}

static void ushc_disconnect(struct usb_interface *intf)
{
    struct sdio_slot *slot = usb_get_intfdata(intf);
    struct ushc_data *ushc = slot->drv_data;

    spin_lock(&ushc->lock);
    ushc->disconnected = 1;
    spin_unlock(&ushc->lock);

    usb_kill_urb(ushc->int_urb);
    usb_kill_urb(ushc->cmd_urb);
    usb_kill_urb(ushc->data_urb);
    usb_kill_urb(ushc->resp_urb);

    sdio_slot_unregister(slot);

    ushc_clean_up(slot);
}

static struct usb_device_id ushc_id_table[] = {
    /* CSR USB SD Host Controller */
    { USB_DEVICE(0x0a12, 0x5d10) }, /* FIXME: real device ID TBD */
    { USB_DEVICE(0x04b4, 0x4611) },
    { },
};

static struct usb_driver ushc_driver = {
    .name       = "slot_ushc",
    .id_table   = ushc_id_table,
    .probe      = ushc_probe,
    .disconnect = ushc_disconnect,
};

static int __init ushc_init(void)
{
    return usb_register(&ushc_driver);
}
module_init(ushc_init);

static void __exit ushc_exit(void)
{
    usb_deregister(&ushc_driver);
}
module_exit(ushc_exit);

MODULE_DESCRIPTION("USB SD Host Controller slot driver");
MODULE_AUTHOR("Cambridge Silicon Radio Ltd.");
MODULE_LICENSE("GPL and additional rights");
