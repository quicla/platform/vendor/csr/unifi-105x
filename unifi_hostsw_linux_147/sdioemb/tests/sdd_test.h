#ifndef LIBSDIO_TEST_H
#define LIBSDIO_TEST_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>

#include <sdioemb/libsdio.h>
#include <sdioemb/sdio.h>
#include <sdioemb/sdio_csr.h>

struct sdd_test_card {
    uint16_t manf_id;
    uint16_t card_id;
    const char *name;
    void *card_data;
};

typedef void (*sdd_test_func_t)(sdio_uif_t uif, void *arg);

int sdd_test_run(const char *test, int argc, char *argv[],
                 sdd_test_func_t test_func, void *test_arg,
                 sdio_int_handler_t int_handler, void *int_arg);

void *sdd_test_check_cards(const struct sdd_test_card *cards);

void sdd_test_pass(void);
void sdd_test_fail(const char *fmt, ...);
void sdd_test_abort(const char *fmt, ...);

int sdd_test_max_iters(void);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* #ifndef LIBSDIO_TEST_H */
