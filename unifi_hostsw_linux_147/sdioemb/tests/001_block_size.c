/*
 * 001_block_size - test set/get block size.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * Tests setting default and power of two block sizes from 8 to maximum
 * supported by card.
 */
#include "sdd_test.h"

static void test_001_block_size(sdio_uif_t uif, void *arg)
{
    int num_funcs;
    int f;

    num_funcs = sdio_num_functions(uif);

    for (f = 0; f <= num_funcs; f++) {
        int max_block_size;
        int block_size;
        int new_block_size;
        int ret;

        max_block_size = sdio_max_block_size(uif, f);

        if (max_block_size == 0) {
            continue;
        }

        /*
         * Test block sizes up to the maximum.
         */
        for (block_size = 4; block_size <= max_block_size; block_size *= 2) {
            ret = sdio_set_block_size(uif, f, block_size);
            if (ret < 0) {
                sdd_test_fail("F%d: set block size to %d", f, block_size);
            }

            new_block_size = sdio_block_size(uif, f);
            if (new_block_size != block_size) {
                sdd_test_fail("F%d: new block size (%d) != requested block size %d\n",
                              f, new_block_size, block_size);
            }
        }

        /*
         * Set default block size and check it's the expected size.
         */
        ret = sdio_set_block_size(uif, f, 0);
        if (ret < 0) {
            sdd_test_fail("F%d: set block size to default", f);
        }

        if (max_block_size > 512) {
            block_size = 512;
        } else {
            block_size = max_block_size;
        }

        new_block_size = sdio_block_size(uif, f);
        if (new_block_size != block_size) {
            sdd_test_fail("F%d: new block size (%d) != requested block size %d\n",
                          f, new_block_size, block_size);
        }
    }

    sdd_test_pass();
}

int main(int argc, char *argv[])
{
    return sdd_test_run(SDD_TEST_NAME, argc, argv,
                        test_001_block_size, NULL, NULL, NULL);
}
