/*
 * sdioemb test infrastructure.
 *
 * Copyright (C) 2008 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>
#include <semaphore.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>

#include "sdd_test.h"

struct sdd_test {
    const char *name;
    int argc;
    char **argv;
    sdio_uif_t uif;
    sdd_test_func_t test_func;
    void *test_arg;
    sdio_int_handler_t int_handler;
    void *int_arg;
    int max_iters;
    sem_t test_done_sem;
    sem_t halt_test_sem;
    int ret;
};

static struct sdd_test sdd;

static void sdd_test_abort_usage()
{
    sdd_test_abort("usage: %s [-i <iters>] <sdio uif device> [test specific options]",
                   sdd.argv[0]);
}

static void parse_options(void)
{
    opterr = 0;

    for (;;) {
        unsigned n;

        switch (getopt(sdd.argc, sdd.argv, "+i:")) {
        case 'i':
            if (sscanf(optarg, "%u", &n) < 1) {
                sdd_test_abort("invalid parameter '%s' to -i option", optarg);
            }
            sdd.max_iters = n;
            break;
        case '?':
            sdd_test_abort_usage();
            break;
        case -1:
            sdd.argv[optind-1] = sdd.argv[0];
            sdd.argc -= optind - 1;
            sdd.argv += optind - 1;
            return;
        }
    }
}

static void *test_thread_func(void *arg)
{
    parse_options();

    if (sdd.argc < 2) {
        sdd_test_abort_usage();
    }

    sdd.uif = sdio_open(sdd.argv[1], sdd.int_handler, sdd.int_arg);
    if (sdd.uif == NULL) {
        sdd_test_abort("%s", sdd.argv[1]);
    }

    sdd.test_func(sdd.uif, sdd.test_arg);

    pthread_exit(0);
}

int sdd_test_run(const char *test, int argc, char *argv[],
                 sdd_test_func_t test_func, void *test_arg,
                 sdio_int_handler_t int_handler, void *int_arg)
{
    pthread_t test_thread;
    int ret;

    sdd.name = test;
    sdd.argc = argc;
    sdd.argv = argv;
    sdd.test_func = test_func;
    sdd.test_arg = test_arg;
    sdd.int_handler = int_handler;
    sdd.int_arg = int_arg;
    sdd.max_iters = 10;

    sem_init(&sdd.test_done_sem, 0, 0);
    sem_init(&sdd.halt_test_sem, 0, 0);

    srand(time(NULL));

    ret = pthread_create(&test_thread, NULL, test_thread_func, NULL);
    if (ret < 0) {
        sdd_test_abort("pthread_create");
    }

    sem_wait(&sdd.test_done_sem);

    if (sdd.uif) {
        sdio_close(sdd.uif);
    }
    return sdd.ret;
}

static void sdd_test_exit(int ret)
{
    sdd.ret = ret;
    sem_post(&sdd.test_done_sem);
    sem_wait(&sdd.halt_test_sem);
}

void sdd_test_pass(void)
{
    printf("%s: PASS\n", sdd.name);

    sdd_test_exit(0);
}

void sdd_test_fail(const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    printf("%s: FAIL: ", sdd.name);
    vprintf(fmt, args);
    if (errno) {
        printf(": %s\n", strerror(errno));
    } else {
        printf("\n");
    }
    va_end(args);

    sdd_test_exit(1);
}

void sdd_test_abort(const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    printf("%s: ABORT: ", sdd.name);
    vprintf(fmt, args);
    if (errno) {
        printf(": %s\n", strerror(errno));
    } else {
        printf("\n");
    }
    va_end(args);

    sdd_test_exit(2);
}

void *sdd_test_check_cards(const struct sdd_test_card *cards)
{
    const struct sdd_test_card *c;

    c = cards;
    while (c->name) {
        if (sdio_manf_id(sdd.uif) == c->manf_id && sdio_card_id(sdd.uif) == c->card_id) {
            return c->card_data;
        }
        c++;
    }

    printf("%s: INFO: supported cards are:\n", sdd.name);
    c = cards;
    while (c->name) {
        printf("  %04x:%04x (%s)\n", c->manf_id, c->card_id, c->name);
        c++;
    }
    sdd_test_abort("required card not present");
    return NULL;
}

int sdd_test_max_iters(void)
{
    return sdd.max_iters;
}
