/*
 * Utility for force an SDIO card reinsert.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include <stdio.h>
#include <stdlib.h>

#include <sdioemb/libsdio.h>

int main(int argc, char *argv[])
{
    sdio_uif_t sdev;

    if (argc < 2) {
        fprintf(stderr, "Usage: %s <sdio device>\n", argv[0]);
        exit(1);
    }

    sdev = sdio_open(argv[1], NULL, NULL);
    if (!sdev) {
        perror("sdio_open");
        exit(1);
    }

    sdio_reinsert_card(sdev);

    return 0;
}
