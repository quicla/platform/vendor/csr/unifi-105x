/*
 * Utility to print the card and function CIS.
 *
 * Copyright (C) 2008 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include <sdioemb/libsdio.h>
#include <sdioemb/sdio.h>
#include <sdioemb/sdio_cis.h>

static const char *sdio_tpl_str(uint8_t tpl)
{
    static const char *tpls[256] = {
        [CISTPL_NULL] = "NULL",
        [CISTPL_CHECKSUM] = "CHECKSUM",
        [CISTPL_VERS_1] = "VERS_1",
        [CISTPL_ALTSTR] = "ALTSTR",
        [CISTPL_MANFID] = "MANFID",
        [CISTPL_FUNCID] = "FUNCID",
        [CISTPL_FUNCE] = "FUNCE",
        [CISTPL_SDIO_STD] = "SDIO_STD",
        [CISTPL_SDIO_EXT] = "SDIO_EXT",
    };
    
    if (tpls[tpl] == NULL) {
        return "UNKNOWN";
    }
    return tpls[tpl];
}

int sdio_cis_read_ptr_reg(sdio_uif_t uif, uint32_t addr, uint32_t *ptr)
{
    uint32_t cis_ptr = 0;
    int b;

    for (b = 0; b < 3; b++) {
        uint8_t p;
        int ret = sdio_read8(uif, 0, addr + b, &p);
        if (ret < 0) {
            return ret;
        }
        cis_ptr |= p << (b * 8);
    }
    *ptr = cis_ptr;
    return 0;
}

int sdio_print_tpl(sdio_uif_t uif, uint8_t tpl, uint8_t lnk, uint32_t cis_ptr)
{
    int ret = 0;
    uint8_t val;
    int i;

    printf("  %s(%02x)", sdio_tpl_str(tpl), tpl);
    for (i = 0; i < lnk; i++) {
        if (i % 8 == 0) {
            printf("\n    ");
        }
        ret = sdio_read8(uif, 0, cis_ptr++, &val);
        if (ret < 0) {
            break;
        }
        printf(" %02x", val);
    }
    printf("\n");

    return ret;
}

int sdio_print_cis(sdio_uif_t uif, int func)
{
    uint32_t cis_ptr_addr;
    uint32_t cis_ptr;
    uint8_t tpl, lnk;
    int ret;

    if (func == 0) {
        cis_ptr_addr = SDIO_CCCR_CIS_PTR;
    } else {
        cis_ptr_addr = SDIO_FBR_CIS_PTR(func);
    }

    ret = sdio_cis_read_ptr_reg(uif, cis_ptr_addr, &cis_ptr);
    if (ret < 0) {
        return ret;
    }
    if (cis_ptr < 0x1000 || cis_ptr > 0x17000) {
        errno = EINVAL;
        return -1;
    }

    for (;;) {
        if (cis_ptr >= 0x17000) {
            /* A valid CIS should have a CISTPL_END so this shouldn't happen. */
            errno = EINVAL;
            return -1;
        }

        ret = sdio_read8(uif, 0, cis_ptr++, &tpl);
        if (ret < 0) {
            return ret; 
        }
        ret = sdio_read8(uif, 0, cis_ptr++, &lnk);
        if (ret < 0) {
            return ret;
        }

        if (tpl == CISTPL_END) {
            break;
        }

        ret = sdio_print_tpl(uif, tpl, lnk, cis_ptr);
        if (ret < 0) {
            return ret;
        }

        cis_ptr += lnk;
    }
    return 0;
}

int main(int argc, char *argv[])
{
    sdio_uif_t uif;
    int func;

    if (argc < 2) {
        fprintf(stderr, "Usage: %s <sdio device>\n", argv[0]);
        exit(1);
    }

    uif = sdio_open(argv[1], NULL, NULL);
    if (!uif) {
        perror("sdio_open");
        exit(1);
    }

    for (func = 0; func <= sdio_num_functions(uif); func++) {
        int ret;

        printf("Function %d\n", func);
        ret = sdio_print_cis(uif, func);
        if (ret < 0) {
            perror("sdio_print_cis");
            exit(1);
        }
    }

    return 0;
}
