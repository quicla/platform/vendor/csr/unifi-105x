/*
 * Utility to control the SD bus clock frequency.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include <stdio.h>
#include <stdlib.h>

#include <sdioemb/libsdio.h>

int main(int argc, char *argv[])
{
    sdio_uif_t sdev;
    int freq;

    if (argc < 3) {
        fprintf(stderr, "Usage: %s <sdio device> <clock freq>\n", argv[0]);
        exit(1);
    }

    sdev = sdio_open(argv[1], NULL, NULL);
    if (!sdev) {
        perror("sdio_open");
        exit(1);
    }
    freq = atoi(argv[2]);
    sdio_set_max_bus_freq(sdev, freq);

    return 0;
}
