/*
 * libsdio asynchronous I/O functions.
 *
 * Copyright (C) 2008 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <pthread.h>
#include <assert.h>

#include <linux/sdioemb/uif.h>
#include <sdioemb/libsdio.h>

#include "libsdio_internal.h"

static void *async_thread_func(void *arg);

int async_thread_start(sdio_uif_t uif)
{
    int ret;

    pthread_cond_init(&uif->cmd_queue.cond, NULL);

    uif->cmd_queue.head = NULL;
    uif->cmd_queue.tail = NULL;

    ret = pthread_create(&uif->async_thread, NULL, async_thread_func, uif);
    if (ret < 0) {
        return ret;
    }

    return 0;
}

void async_thread_stop(sdio_uif_t uif)
{
    struct cmd_queue_elem *elem;

    pthread_mutex_lock(&uif->mutex);
    elem = uif->cmd_queue.head;
    while (elem) {
        struct cmd_queue_elem *next = elem->next;
        free(elem);
        elem = next;
    }
    pthread_mutex_unlock(&uif->mutex);

    pthread_cancel(uif->async_thread);
    pthread_join(uif->async_thread, NULL);
}

static int cmd_queue_push(sdio_uif_t uif, enum sdio_uif_cmd_type type, int func, uint32_t addr,
                          const uint8_t *data, size_t len, int block_size,
                          sdio_io_callback_t callback, void *arg)
{
    struct cmd_queue_elem *elem;

    elem = malloc(sizeof(struct cmd_queue_elem));
    if (elem == NULL) {
        return -1;
    }

    elem->callback = callback;
    elem->arg = arg;

    if (type == SDD_UIF_CMD52_WRITE) {
        elem->write8_data = *data;
        data = &elem->write8_data;
    }

    elem->cmd.type       = type;
    elem->cmd.function   = func;
    elem->cmd.address    = addr;
    elem->cmd.data       = (uint8_t *)data; /* const cast is safe */
    elem->cmd.len        = len;
    elem->cmd.block_size = block_size;

    pthread_mutex_lock(&uif->mutex);
    elem->next = NULL;
    if (uif->cmd_queue.tail != NULL) {
        uif->cmd_queue.tail->next = elem;
    }
    if (uif->cmd_queue.head == NULL) {
        uif->cmd_queue.head = elem;
    }
    uif->cmd_queue.tail = elem;
    pthread_mutex_unlock(&uif->mutex);
    pthread_cond_signal(&uif->cmd_queue.cond);

    return 0;
}

static struct cmd_queue_elem *cmd_queue_pop(sdio_uif_t uif)
{
    struct cmd_queue_elem *elem;

    pthread_mutex_lock(&uif->mutex);
    while (uif->cmd_queue.head == NULL) {
        pthread_cond_wait(&uif->cmd_queue.cond, &uif->mutex);
    }

    elem = uif->cmd_queue.head;
    uif->cmd_queue.head = NULL;
    if (uif->cmd_queue.head == NULL) {
        uif->cmd_queue.tail = NULL;
    }
    pthread_mutex_unlock(&uif->mutex);

    return elem;
}

static void *async_thread_func(void *arg)
{
    struct sdio_uif *uif = arg;

    for (;;) {
        struct cmd_queue_elem *elem;
        int ret;

        elem = cmd_queue_pop(uif);
        ret = ioctl(uif->fd, SDD_UIF_IOCCMD, &elem->cmd);
        pthread_testcancel();
        elem->callback(uif, elem->arg, ret);
    }

    pthread_exit(0);
}

int sdio_read8_async(sdio_uif_t uif, int func, uint32_t addr, uint8_t *data,
                     sdio_io_callback_t callback, void *arg)
{
    return cmd_queue_push(uif, SDD_UIF_CMD52_READ, func, addr, data, 1, 0, callback, arg);
}

int sdio_write8_async(sdio_uif_t uif, int func, uint32_t addr, uint8_t data,
                      sdio_io_callback_t callback, void *arg)
{
    return cmd_queue_push(uif, SDD_UIF_CMD52_WRITE, func, addr, &data, 1, 0, callback, arg);
}

int sdio_read_async(sdio_uif_t uif, int func, uint32_t addr, uint8_t *data,
                    size_t len, int block_size,
                    sdio_io_callback_t callback, void *arg)
{
    return cmd_queue_push(uif, SDD_UIF_CMD53_READ, func, addr, data, len, block_size,
                          callback, arg);
}

int sdio_write_async(sdio_uif_t uif, int func, uint32_t addr, const uint8_t *data,
                     size_t len, int block_size,
                     sdio_io_callback_t callback, void *arg)
{
    return cmd_queue_push(uif, SDD_UIF_CMD53_WRITE, func, addr, data, len, block_size,
                          callback, arg);
}
