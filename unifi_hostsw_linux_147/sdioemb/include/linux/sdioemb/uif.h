/*
 * Userspace interface to the SDIO Userspace Interface driver.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#ifndef LINUX_SDIOEMB_UIF_H
#define LINUX_SDIOEMB_UIF_H

enum sdio_uif_cmd_type {
    SDD_UIF_CMD52_READ, SDD_UIF_CMD52_WRITE,
    SDD_UIF_CMD53_READ, SDD_UIF_CMD53_WRITE,
};

struct sdio_uif_cmd {
    enum sdio_uif_cmd_type type;
    int                    function;
    uint32_t               address;
    uint8_t *              data;
    size_t                 len;
    int                    block_size;
};

#define SDD_UIF_IOC_MAGIC 's'

#define SDD_UIF_IOCQNUMFUNCS  _IO(SDD_UIF_IOC_MAGIC,   0)
#define SDD_UIF_IOCCMD        _IOWR(SDD_UIF_IOC_MAGIC, 1, struct sdio_uif_cmd)
#define SDD_UIF_IOCWAITFORINT _IO(SDD_UIF_IOC_MAGIC,   2)
#define SDD_UIF_IOCTBUSWIDTH  _IO(SDD_UIF_IOC_MAGIC,   3)
#define SDD_UIF_IOCREINSERT   _IO(SDD_UIF_IOC_MAGIC,   4)
#define SDD_UIF_IOCTBUSFREQ   _IO(SDD_UIF_IOC_MAGIC,   5)
#define SDD_UIF_IOCQMANFID    _IO(SDD_UIF_IOC_MAGIC,   6)
#define SDD_UIF_IOCQCARDID    _IO(SDD_UIF_IOC_MAGIC,   7)
#define SDD_UIF_IOCQSTDIF     _IO(SDD_UIF_IOC_MAGIC,   8)
#define SDD_UIF_IOCQMAXBLKSZ  _IO(SDD_UIF_IOC_MAGIC,   9)
#define SDD_UIF_IOCQBLKSZ     _IO(SDD_UIF_IOC_MAGIC,  10)
#define SDD_UIF_IOCTBLKSZ     _IO(SDD_UIF_IOC_MAGIC,  11)

#endif /* #ifndef LINUX_SDIOEMB_UIF_H */
