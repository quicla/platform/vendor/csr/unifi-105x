/*
 * Slot driver API.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#ifndef _SLOT_API_H
#define _SLOT_API_H

#include <sdioemb/sdio_api.h>

/**
 * @defgroup sdriver SDIO slot driver API
 *
 * @brief The SDIO slot driver API provides an interface for the SDIO
 * layer to driver an SDIO slot (socket).
 *
 * Slot drivers register with the SDIO layer (sdio_slot_register()),
 * providing functions to starting commands, enabling/disable card
 * interrupts, card detection and bus power control.
 *
 * Functions are provided to notify the SDIO layer when a command has
 * completed (sdio_cmd_complete()) and when an SDIO card interrupt has
 * occurred (sdio_interrupt()).
 */

#define SDD_BUS_FREQ_OFF      0
#define SDD_BUS_FREQ_DEFAULT -1
#define SDD_BUS_FREQ_IDLE    -2

/**
 * Valid SDIO bus voltage levels.
 *
 * @ingroup sdriver
 */
enum sdio_power {
    SDIO_POWER_OFF  =   0, /**< Power switched off. */
    SDIO_POWER_3V3  =  33, /**< Voltage set to 3.3V. */
};

/**
 * SDIO slot capabilities.
 *
 * @ingroup sdriver
 */
struct slot_caps {
    int max_bus_freq;  /**< Maximum bus frequency (Hz). */
    int max_bus_width; /**< Maximum bus width supported (1 or 4 data lines). */
    uint8_t cspi_mode; /**< CSPI_MODE register value (for CSPI capable slots). */
};

/**
 * Controller hardware type.
 */
enum slot_controller_type {
    SDD_SLOT_TYPE_SD = 0,   /**< SD/SDIO controller. */
    SDD_SLOT_TYPE_SPI,      /**< SPI controller. */
    SDD_SLOT_TYPE_SPI_CSPI, /**< SPI controller capable of CSPI. */
};

struct sdio_slot_priv;

/**
 * An SDIO slot driver.
 *
 * Allocate and free with sdio_slot_alloc() and sdio_slot_free().
 *
 * @ingroup sdriver
 */
struct sdio_slot {
    /**
     * Name of the slot used in diagnostic messages.
     *
     * This would typically include the name of the SDIO controller
     * and the slot number if the controller has multiple slots.
     */
    char name[64];

    /**
     * Controller hardware type.
     */
    enum slot_controller_type type;

    /**
     * Set the SD bus clock frequency.
     *
     * The driver's implementation should set the SD bus clock to not
     * more than \a clk Hz (unless \a clk is equal to
     * #SDD_BUS_FREQ_OFF or #SDD_BUS_FREQ_IDLE).
     *
     * If \a clk == SDD_BUS_FREQ_OFF the clock should be stopped.
     *
     * \a clk == SDD_BUS_FREQ_IDLE indicates that the bus is idle
     * (currently unused) and the host controller may slow (or stop)
     * the SD bus clock to save power on the card.  During this idle
     * state the host controller must be capable of receiving SDIO
     * interrupts (for certain host controllers this may require
     * leaving the clock running).
     *
     * If \a clk is greater than #SDIO_CLOCK_FREQ_NORMAL_SPD (25 MHz)
     * subsequent commands should be done with the controller in high
     * speed mode.
     *
     * Called from: interrupt context.
     *
     * @param slot  the slot to configure.
     * @param clk   new SD bus clock frequency in Hz, SDD_BUS_FREQ_OFF
     *              or SDD_BUS_FREQ_IDLE.
     *
     * @return The bus frequency actually configured in Hz.
     */
    int (*set_bus_freq)(struct sdio_slot *slot, int clk);

    /**
     * Set the SD bus width.
     *
     * The driver's implementation should set the width of the SD bus
     * for all subsequent data transfers to the specified value.
     *
     * This may be NULL if the driver sets the bus width when starting
     * a command, or the driver is for an SDIO-SPI or CSPI controller.
     *
     * Called from: thread context.
     *
     * @param slot      the slot to configure.
     * @param bus_width new SD bus width (either 1 or 4).
     *
     * @return 0 on success.
     * @return -ve if a low-level error occured when setting the bus width.
     */
    int (*set_bus_width)(struct sdio_slot *slot, int bus_width);

    /**
     * Start an SDIO command.
     *
     * The driver's implementation should:
     *
     *   - set the controller's bus width to #bus_width,
     *   - program the controller to start the command.
     *
     * Called from: interrupt context.
     *
     * @param slot  slot to perform the command.
     * @param cmd   SDIO command to start.
     */
    int (*start_cmd)(struct sdio_slot *slot, struct sdio_cmd *cmd);

    /**
     * Detect if a card is inserted into the slot.
     *
     * Called from: thread context.
     *
     * @param slot slot to check.
     *
     * @return non-zero if a card is inserted; 0 otherwise.
     */
    int (*card_present)(struct sdio_slot *slot);

    /**
     * Switch on/off the SDIO bus power and set the SDIO bus voltage.
     *
     * Called from: thread context.
     *
     * @param slot  the slot.
     * @param power the requested voltage.
     *
     * @return 0 on success; -ve on error: -EINVAL - requested voltage
     * is not supported.
     */
    int (*card_power)(struct sdio_slot *slot, enum sdio_power power);

    /**
     * Enable (unmask) the SDIO card interrupt on the controller.
     *
     * Called from: interrupt context.
     *
     * @param slot the slot to enable the interrupt on..
     */
    void (*enable_card_int)(struct sdio_slot *slot);

    /**
     * Disable (mask) the SDIO card interrupt on the controller.
     *
     * Called from: thread context.
     *
     * @param slot the slot to disable the interrupt on.
     */
    void (*disable_card_int)(struct sdio_slot *slot);

    /**
     * Perform a hard reset of the card.
     *
     * Hard resets can be achieved in two ways:
     *
     * -# Power cycle (if the slot has power control).
     * -# Platform-specific assertion of a card/chip reset line.
     *
     * If hard resets are not supported, either return 0 or set
     * hard_reset to NULL.
     *
     * @param slot the slot for the card to reset.
     *
     * @return 0 if a hard reset was performed.
     * @return 1 if hard resets are not supported.
     */
    int (*hard_reset)(struct sdio_slot *slot);

    struct slot_caps         caps;           /**< Slot capabilities. */
    int                      clock_freq;     /**< SD bus frequency requested by the SDIO layer. */
    int                      bus_width;      /**< Bus width requested by the SDIO layer. */
    int                      cspi_reg_pad;   /**< Padding for CSPI register reads. */
    int                      cspi_burst_pad; /**< Padding for CSPI burst reads. */
    struct sdio_slot_priv *priv;           /**< Data private to the SDIO layer. */
    void *                   drv_data;       /**< Data private to the slot driver. */
};

struct sdio_slot *sdio_slot_alloc(size_t drv_data_size);
void sdio_slot_free(struct sdio_slot *slot);
int sdio_slot_register(struct sdio_slot *slot);
void sdio_slot_unregister(struct sdio_slot *slot);
void sdio_card_inserted(struct sdio_slot *slot);
void sdio_card_removed(struct sdio_slot *slot);
void sdio_interrupt(struct sdio_slot *slot);
void sdio_cmd_complete(struct sdio_slot *slot, struct sdio_cmd *cmd);

void sdio_suspend(struct sdio_slot *slot);
void sdio_resume(struct sdio_slot *slot);


#endif /* #ifndef _SLOT_API_H */
