/*
 * Power management.
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#include "sdio_layer.h"

/**
 * Request power to the card.
 *
 * If the card is not powered, apply power and re-initialize the card.
 * No per-function state of the card is restored and the driver should
 * re-enable the function and perform any other function specific
 * initialization.
 *
 * This is called by the core before calling a function driver's
 * probe() method.
 *
 * @param fdev the SDIO device requesting power.
 *
 * @ingroup fdriver
 */
void sdio_power_on(struct sdio_dev *fdev)
{
    struct sdio_slot *slot = fdev->priv->slot;
    struct sdio_slot_priv *slotp = slot->priv;

    os_mutex_lock(&slotp->card_mutex);

    slotp->power |= 1 << fdev->function;

    if (!slotp->card_powered) {
        sdio_card_start(slot);
        sdio_card_configure(slot);
        sdio_func_set_max_bus_freq(fdev, fdev->priv->max_freq);
    }

    os_mutex_unlock(&slotp->card_mutex);
}

/**
 * Allow the card to be powered off.
 *
 * If no other function is requesting power, the card is powered
 * off. The driver must call sdio_power_on() before performing any
 * more I/O to the card.
 *
 * Interrupts are disabled by calling sdio_disable_interrupt() as the
 * DAT1/INT line may no longer be pulled high when power is removed.
 *
 * This is called by the core after calling a function driver's
 * remove() method.
 *
 * @param fdev the SDIO device not requiring power.
 *
 * @ingroup fdriver
 */
void sdio_power_off(struct sdio_dev *fdev)
{
    struct sdio_slot *slot = fdev->priv->slot;
    struct sdio_slot_priv *slotp = slot->priv;

    sdio_disable_interrupt(fdev);

    os_mutex_lock(&slotp->card_mutex);

    slotp->power &= ~(1 << fdev->function);

    if (!slotp->power && slotp->card_powered) {
        sdio_card_stop(slot);
    }

    os_mutex_unlock(&slotp->card_mutex);
}

/**
 * Notify the SDIO driver that the suspend event has been received.
 *
 * @param slot the slot being suspended.
 * 
 * @ingroup sdriver
 */
void sdio_suspend(struct sdio_slot *slot)
{
    struct sdio_slot_priv *slotp = slot->priv;
    int f;

    /*
     * Pass the event to all device drivers that
     * have registered a suspend handler.
     */
    for (f = 0; f < SDD_MAX_FUNCTIONS; f++) {
        struct sdio_dev *fdev = slotp->functions[f];
        if (fdev) {
            if (fdev->driver) {
                if (fdev->driver->suspend) {
                    fdev->driver->suspend(fdev);
                }
            }
        }
    }
    slot_set_bus_freq(slot, SDD_BUS_FREQ_OFF);
}


/**
 * Notify the SDIO driver that the resume event has been received.
 *
 * @param slot the slot being resumed.
 * 
 * @ingroup sdriver
 */
void sdio_resume(struct sdio_slot *slot)
{
    struct sdio_slot_priv *slotp = slot->priv;
    int f;

    /*
     * Pass the event to all device drivers that
     * have registered a resume handler.
     */
    for (f = 0; f < SDD_MAX_FUNCTIONS; f++) {
        struct sdio_dev *fdev = slotp->functions[f];
        if (fdev) {
            if (fdev->driver) {
                if (fdev->driver->resume) {
                    fdev->driver->resume(fdev);
                }
            }
        }
    }
}
