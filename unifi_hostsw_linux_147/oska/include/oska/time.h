/*
 * Operating system kernel abstraction -- timing
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#ifndef __OSKA_TIME_H
#define __OSKA_TIME_H

#ifdef __OSKA_API_DOC
/**
 * @defgroup time Timing and delays
 */

/**
 * Return the current time (since an arbitrary point) in milliseconds.
 *
 * The resolution depends on the OS but would typically be 10 ms or
 * better.
 *
 * Callable from: any context.
 *
 * @return current time in ms.
 *
 * @ingroup time
 */
unsigned long os_current_time_ms(void);

/**
 * Sleep for a minimum length of time.
 *
 * Callable from: thread context.
 *
 * @param ms minimum time to sleep (in ms).
 *
 * @ingroup time
 */
int os_sleep_ms(unsigned ms);

/**
 * Delay a minimum length of time.
 *
 * This may busy-wait and should not be used for long delays (use
 * os_sleep_ms() instead).
 *
 * Callable from: any context.
 *
 * @param us minimum time to delay (in us).
 *
 * @ingroup time
 */
int os_delay_us(unsigned us);

#endif /* __OSKA_API_DOC */

#ifdef linux
#  include <../linux/time.h>
#else
#  error <oska/time.h> not provided for this OS
#endif

#endif /* #ifndef __OSKA_TIME_H */
