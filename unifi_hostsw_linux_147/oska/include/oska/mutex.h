/*
 * Operating system kernel abstraction -- mutexes
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#ifndef __OSKA_MUTEX_H
#define __OSKA_MUTEX_H

#ifdef __OSKA_API_DOC
/**
 * @defgroup mutex Mutexes
 */

/**
 * Implementation defined mutex object.
 *
 * @ingroup mutex
 */
typedef implementation_defined os_mutex_t;

/**
 * Initialize a mutex.
 *
 * Callable from: thread context.
 *
 * @param mutex mutex to initialize.
 *
 * @ingroup mutex
 */
void os_mutex_init(os_mutex_t *mutex);

/**
 * Lock a mutex.
 *
 * Callable from: thread context.
 *
 * @param mutex the mutex to lock.
 *
 * @ingroup mutex
 */
void os_mutex_lock(os_mutex_t *mutex);

/**
 * Unlock a mutex.
 *
 * Callable from: thread context.
 *
 * @param mutex the mutex to unlock.
 *
 * @ingroup mutex
 */
void os_mutex_unlock(os_mutex_t *mutex);

#endif /* __OSKA_API_DOC */

#ifdef linux
#  include <../linux/mutex.h>
#else
#  error <oska/mutex.h> not provided for this OS
#endif

#endif /* #ifndef __OSKA_MUTEX_H */
