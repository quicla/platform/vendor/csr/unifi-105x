/*
 * Operating system kernel abstraction -- threading
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#ifndef __OSKA_THREAD_H
#define __OSKA_THREAD_H

#include <oska/semaphore.h>

#ifdef __OSKA_API_DOC
/**
 * @defgroup thread Threading
 *
 * Threading support depends on the semaphore API provided by
 * <oska/semaphore.h> (see \ref semaphore).
 */

/**
 * Implementation defined thread object.
 *
 * @ingroup thread
 */
typedef implementation_defined os_thread_t;

/**
 * Create and start a thread.
 *
 * The thread function should call os_thread_should_stop() and return
 * when it returns true.
 *
 * Callable from: thread context.
 *
 * @param thread pointer to OS-specific thread data.
 * @param name   thread name.
 * @param func   thread entry-point function.
 * @param arg    argument for func.
 *
 * @return 0 on success; negative error code otherwise.
 *
 * @ingroup thread
 */
int os_thread_create(os_thread_t *thread, const char *name, void (*func)(void *), void *arg);

/**
 * Stop a thread and wait for it to exit.
 *
 * If \a sem is non-NULL it will be posted to awaken the thread. The
 * thread will also be awakened if it's asleep (in a os_sleep_ms()
 * call for example).
 *
 * Callable from: thread context.
 *
 * @param thread the thread to stop.
 * @param sem    a semaphore to post to awaken the thread, may be NULL.
 *
 * @return 0 on success; -ve otherwise.
 *
 * @ingroup thread
 */
void os_thread_stop(os_thread_t *thread, os_semaphore_t *sem);

/**
 * Return true if the thread should stop (as signalled by a call to
 * os_thread_stop()).
 *
 * @param thread the thread to check.
 *
 * @return true iff the thread should stop.
 *
 * @ingroup thread
 */
int os_thread_should_stop(os_thread_t *thread);

/**
 * Try to suspend a thread.
 *
 * This specifies a point where it is safe for a thread to be
 * suspended.
 *
 * Callable from: thread context.
 *
 * @param thread pointer to OS-specific thread data.
 *
 * @ingroup thread
 */
void os_try_suspend_thread(os_thread_t *thread);

#endif /* __OSKA_API_DOC */

#ifdef linux
#  include <../linux/thread.h>
#else
#  error <oska/thread.h> not provided for this OS
#endif

#endif /* #ifndef __OSKA_THREAD_H */
