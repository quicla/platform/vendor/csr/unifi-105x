/*
 * OSKA Linux implementation -- semaphores
 *
 * Copyright (C) 2007 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 */
#ifndef __OSKA_LINUX_SEMAPHORE_H
#define __OSKA_LINUX_SEMAPHORE_H

#include <linux/kernel.h>

#include <linux/kernel-compat.h>

typedef struct semaphore os_semaphore_t;

static inline void os_semaphore_init(os_semaphore_t *sem)
{
    sema_init(sem, 0);
}

static inline void os_semaphore_wait(os_semaphore_t *sem)
{
    down(sem);
}

static inline void os_semaphore_post(os_semaphore_t *sem)
{
    up(sem);
}

#endif /* __OSKA_LINUX_SEMAPHORE_H */
