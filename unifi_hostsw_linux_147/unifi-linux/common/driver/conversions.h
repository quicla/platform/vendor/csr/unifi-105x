/*
 * ---------------------------------------------------------------------------
 *
 * FILE: conversions.h
 * 
 * PURPOSE:
 *      This header file provides the macros for converting to and from
 *      wire format.
 *      These macros *MUST* work for little-endian AND big-endian hosts.
 *
 * Copyright (C) 2006-2008 by Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * ---------------------------------------------------------------------------
 */

#define SIZEOF_UINT16           2
#define SIZEOF_UINT32           4
#define SIZEOF_UINT64           8

#define SIZEOF_SIGNAL_HEADER    6
#define SIZEOF_DATAREF          4


/* 
 * Note ptr must be a (uint8 *).
 */
#define UNPACK16(ptr, index)        (((ptr)[(index) + 1] << 8) + (ptr)[(index)])

#define UNPACK32(ptr, index)        ((ptr[index + 3] << 24) + \
                                     (ptr[index + 2] << 16) + \
                                     (ptr[index + 1] << 8) + \
                                     ptr[index])

#define UNPACK64(ptr, index)        (((uint64)ptr[index + 7] << 56) + \
                                     ((uint64)ptr[index + 6] << 48) + \
                                     ((uint64)ptr[index + 5] << 40) + \
                                     ((uint64)ptr[index + 4] << 32) + \
                                     ((uint64)ptr[index + 3] << 24) + \
                                     ((uint64)ptr[index + 2] << 16) + \
                                     ((uint64)ptr[index + 1] << 8) + \
                                      (uint64)ptr[index])

#define LE_16(dest, value)      do { \
                                    (dest)[1] = (uint8)((value >> 8) & 0xFF); \
                                    (dest)[0] = (uint8)(value & 0xFF); \
                                } while (0)
                                
#define LE_32(dest, value)      do { \
                                    (dest)[3] = (uint8)((value >> 24) & 0xFF); \
                                    (dest)[2] = (uint8)((value >> 16) & 0xFF); \
                                    (dest)[1] = (uint8)((value >> 8) & 0xFF); \
                                    (dest)[0] = (uint8)(value & 0xFF); \
                                } while (0)
                                
#define LE_64(dest, value)      do { \
                                    (dest)[7] = (uint8)((value >> 56) & 0xFF); \
                                    (dest)[6] = (uint8)((value >> 48) & 0xFF); \
                                    (dest)[5] = (uint8)((value >> 40) & 0xFF); \
                                    (dest)[4] = (uint8)((value >> 32) & 0xFF); \
                                    (dest)[3] = (uint8)((value >> 24) & 0xFF); \
                                    (dest)[2] = (uint8)((value >> 16) & 0xFF); \
                                    (dest)[1] = (uint8)((value >> 8) & 0xFF); \
                                    (dest)[0] = (uint8)(value & 0xFF); \
                                } while (0)


/* 
 * Macro to retrieve the signal ID from a wire-format signal.
 */
#define GET_SIGNAL_ID(_buf)     UNPACK16((_buf), 0)

/* 
 * Macros to retrieve and set the DATAREF fields in a packed (i.e. wire-format)
 * HIP signal.
 */
#define GET_PACKED_DATAREF_SLOT(_buf, _ref)                             \
    UNPACK16((_buf), SIZEOF_SIGNAL_HEADER + ((_ref)*SIZEOF_DATAREF) + 0)

#define GET_PACKED_DATAREF_LEN(_buf, _ref)                              \
    UNPACK16((_buf), SIZEOF_SIGNAL_HEADER + ((_ref)*SIZEOF_DATAREF) + 2)

#define SET_PACKED_DATAREF_SLOT(_buf, _ref, _slot)                      \
    LE_16((_buf) + SIZEOF_SIGNAL_HEADER + ((_ref)*SIZEOF_DATAREF) + 0, (_slot))

#define SET_PACKED_DATAREF_LEN(_buf, _ref, _len)                        \
    LE_16((_buf) + SIZEOF_SIGNAL_HEADER + ((_ref)*SIZEOF_DATAREF) + 2, (_len))

#define GET_PACKED_MA_UNIDATA_STATUS_INDICATION_HOSTTAG(_buf)          \
    UNPACK32((_buf), SIZEOF_SIGNAL_HEADER + UNIFI_MAX_DATA_REFERENCES*SIZEOF_DATAREF + 18)

#define SET_PACKED_MA_UNIDATA_STATUS_INDICATION_HOSTTAG(_buf, _hosttag)          \
    LE_32((_buf) + SIZEOF_SIGNAL_HEADER + UNIFI_MAX_DATA_REFERENCES*SIZEOF_DATAREF + 18, _hosttag)

#define GET_PACKED_MA_UNIDATA_REQUEST_FRAME_PRIORITY(_buf)              \
    UNPACK16((_buf), SIZEOF_SIGNAL_HEADER + UNIFI_MAX_DATA_REFERENCES*SIZEOF_DATAREF + 14)

#define GET_PACKED_MA_UNIDATA_REQUEST_HOSTTAG(_buf)          \
    UNPACK32((_buf), SIZEOF_SIGNAL_HEADER + UNIFI_MAX_DATA_REFERENCES*SIZEOF_DATAREF + 18)

#define SET_PACKED_MA_UNIDATA_REQUEST_HOSTTAG(_buf, _hosttag)          \
    LE_32((_buf) + SIZEOF_SIGNAL_HEADER + UNIFI_MAX_DATA_REFERENCES*SIZEOF_DATAREF + 18, _hosttag)

int get_packed_struct_size(const uint8 *buf);
int read_unpack_signal(const uint8 *ptr, CSR_SIGNAL *sig);
int write_pack(const CSR_SIGNAL *sig, uint8 *ptr, unsigned int *sig_len);
