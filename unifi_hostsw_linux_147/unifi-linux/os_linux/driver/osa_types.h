/** @file osa_types.h
 *
 * Operating System C-Types definition header file
 * 
 *   Copyright (C) Cambridge Silicon Radio Ltd 2006-2008. All rights reserved.
 * 
 * @section DESCRIPTION
 *   Provides definitions of common types such as uint16, uint32, Boolean, etc
 * 
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 ****************************************************************************/
#ifndef OSA_TYPES_H
#   define OSA_TYPES_H

#ifdef __cplusplus
extern "C" {
#endif

/** @{
 * @ingroup abstractions
 */

/* STANDARD INCLUDES ********************************************************/
#ifdef __KERNEL__
#include <linux/version.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/kernel.h>
#include <linux/time.h>
#include <linux/list.h>
#include <linux/wait.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/netdevice.h>
#endif
/* PROJECT INCLUDES *********************************************************/
//#include "osa_support.h"
#include "driver/unifi_types.h"
/* PUBLIC MACROS ************************************************************/

/**
 * This macro allows a function to be declared as non-returning where supported
 * by the compiler and appropriate for the target.
 */
//#ifdef __GNUC__
//#   define OSA_NORETURN            __attribute__ ((__noreturn__))
//#else
#   define OSA_NORETURN
//#endif /* __GNUC__ */


#if 0
/** int8 definition */
typedef signed char     int8;

/** int16 definition */
typedef signed short    int16;
/** int32 definition */
typedef signed long     int32;

/** uint8 definition */
typedef unsigned char   uint8;
/** uint16 definition */
typedef unsigned short  uint16;
/** uint32 definition */
typedef unsigned long   uint32;

/* Types 64 bit types can be very platform specific */
#ifdef SME_64BIT_INTRINSIC
/** int64 definition */
typedef long long int64;
/** uint64 definition */
typedef unsigned long long uint64;
#else
/** int64 definition */
typedef struct int64 
{ 
    /** int64 - part a int32 definition */
    int32 a; 
    /** int64 - part b int32 definition */
    int32 b; 
} int64;
/** uint64 definition */
typedef struct uint64 
{ 
    /** uint64 - part b int32 definition */
    uint32 a; 
    /** uint64 - part b int32 definition */
    uint32 b; 
} uint64;
#endif

/** Boolean definition */
typedef int16   Boolean;
/** HAVE_BOOLEAN_TYPE definition */
#define HAVE_BOOLEAN_TYPE   1

#endif /* 0 */

#ifndef __cplusplus
#ifdef __KERNEL__
/** bool definition */
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,19)
typedef int16   bool;   /* only used by SDL */
#endif
#endif
#endif

/* Boolean values (never test for equality with TRUE) */
#ifndef FALSE
/** FALSE */
#define FALSE           0
#endif

#ifndef TRUE
/** TRUE */
#define TRUE            1
#endif


/* Null pointer (also in <stddef.h>, <stdio.h>, <stdlib.h> and <string.h>) */
#ifndef NULL
/** NULL */
#define NULL            ((void*)0)
#endif

/* Inlined minimum, maximum and absolute (may evaluate expressions twice) */
#ifndef MAX
/** MAX */
#define MAX(a,b)        (((a) < (b)) ? (b) : (a))
#endif

#ifndef MIN
/** MIN */
#define MIN(a,b)        (((a) < (b)) ? (a) : (b))
#endif

#ifndef ABS
/** ABS */
#define ABS(a)          (((a) < 0) ? -(a) : (a))
#endif

 
#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* OSA_H */
