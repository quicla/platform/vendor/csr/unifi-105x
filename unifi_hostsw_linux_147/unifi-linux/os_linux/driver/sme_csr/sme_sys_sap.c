/*
 * ---------------------------------------------------------------------------
 *
 * FILE: sme_sys_sap.c
 *
 * PURPOSE:
 * This file provides the SME SYS API implementation.
 *
 * Copyright (C) 2008 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * ---------------------------------------------------------------------------
 */

#include "unifi_priv.h"
#include "sme_top_level_fsm/sme.h"
#include "sys_sap/sys_sap_build_functions.h"


void unifi_sys_wifi_on_ind(FsmContext* context, unifi_Status status,
                           const unifi_Versions* versions)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_sys_wifi_on_ind(%d)\n", status);

    buflen = build_packed_unifi_sys_wifi_on_ind(&buf, status, versions);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}


void unifi_sys_wifi_on_cfm(FsmContext* context, unifi_Status status)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_sys_wifi_on_cfm(%d)\n", status);

    buflen = build_packed_unifi_sys_wifi_on_cfm(&buf, status);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}


void unifi_sys_wifi_off_ind(FsmContext* context,
                            unifi_ControlIndication controlIndication)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_sys_wifi_off_ind\n");

    buflen = build_packed_unifi_sys_wifi_off_ind(&buf, controlIndication);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}


void unifi_sys_wifi_off_cfm(FsmContext* context)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_sys_wifi_off_cfm\n");

    buflen = build_packed_unifi_sys_wifi_off_cfm(&buf);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}


void unifi_sys_suspend_ind(FsmContext* context, Boolean hardSuspend, Boolean d3Suspend)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_sys_suspend_ind(%d, %d)\n", hardSuspend, d3Suspend);

    buflen = build_packed_unifi_sys_suspend_ind(&buf, hardSuspend, d3Suspend);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}


void unifi_sys_resume_ind(FsmContext* context, Boolean resumePowerMaintained)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2,
                "unifi_sys_resume_ind(%d)\n", resumePowerMaintained);

    buflen = build_packed_unifi_sys_resume_ind(&buf, resumePowerMaintained);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}


void unifi_sys_hip_ind(FsmContext* context, const unifi_DataBlock* mlmeCommand,
                       const unifi_DataBlock* dataRef1,
                       const unifi_DataBlock* dataRef2)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_sys_hip_ind\n");

    buflen = build_packed_unifi_sys_hip_ind(&buf, mlmeCommand,
                                            dataRef1, dataRef2);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}


void unifi_sys_qos_control_cfm(FsmContext* context, unifi_Status status)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_sys_qos_control_cfm(%d)\n", status);

    buflen = build_packed_unifi_sys_qos_control_cfm(&buf, status);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}


void unifi_sys_port_configure_cfm(FsmContext* context, unifi_Status status,
                                  const unifi_MACAddress* macAddress)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_sys_port_configure_cfm(%d)\n", status);

    buflen = build_packed_unifi_sys_port_configure_cfm(&buf, status,
                                                       macAddress);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}

void unifi_sys_traffic_protocol_ind(FsmContext* context,
                                    unifi_traffic_packettype packetType,
                                    unifi_protocol_direction direction,
                                    const unifi_MACAddress* srcAddress)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_sys_traffic_protocol_ind\n");

    buflen = build_packed_unifi_sys_traffic_protocol_ind(&buf,
                                                         packetType,
                                                         direction,
                                                         srcAddress);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}

void unifi_sys_traffic_sample_ind(FsmContext* context, const unifi_traffic_stats* stats)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_sys_traffic_protocol_ind\n");

    buflen = build_packed_unifi_sys_traffic_sample_ind(&buf,stats);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}


void unifi_sys_ip_configured_ind(FsmContext* context, Boolean ipConfigured)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2,
                "unifi_sys_ip_configured_ind(%d)\n", ipConfigured);

    buflen = build_packed_unifi_sys_ip_configured_ind(&buf, ipConfigured);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}


void unifi_sys_multicast_address_ind(FsmContext* context,
                                     unifi_ListAction action,
                                     const unifi_MulticastAddressList* addresses)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_sys_multicast_address_ind\n");

    buflen = build_packed_unifi_sys_multicast_address_ind(&buf,
                                                          action,
                                                          addresses);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}


void unifi_sys_tclas_add_cfm(FsmContext* context, unifi_Status status)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_sys_tclas_add_cfm(%d)\n", status);

    buflen = build_packed_unifi_sys_tclas_add_cfm(&buf, status);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}


void unifi_sys_tclas_del_cfm(FsmContext* context, unifi_Status status)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_sys_tclas_del_cfm(%d)\n", status);

    buflen = build_packed_unifi_sys_tclas_del_cfm(&buf, status);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}

