/* This is an autogenerated file from sme__remote_client_interface_c */
/* Tag: noCheckHeader */

/*    CONFIDENTIAL */
/*    Copyright (C) Cambridge Silicon Radio Ltd 2008. All rights reserved. */

/* This is an autogenerated file from sme__remote_client_interface_c.pl */


#include "fsm/fsm.h" 
#include "fsm/fsm_types.h" 
#include "hostio/hip_fsm_types.h"
#include "smeio/smeio_fsm_types.h"
#include "smeio/smeio_fsm_events.h"
#include "event_pack_unpack/event_pack_unpack.h"
#include "sys_sap/sys_sap.h" 
#include "sys_sap/sys_sap_remote_sme_interface.h" 

static void event_to_unifi_sys_wifi_on_req(FsmContext* context, uint8* inputbuffer, uint16 size)
{
    unifi_sys_wifi_on_req(context);

}

static void event_to_unifi_sys_wifi_on_rsp(FsmContext* context, uint8* inputbuffer, uint16 size)
{
    uint8* buffer = &inputbuffer[6];
    UnifiSysWifiOnRsp_Evt evt;
    /*lint -save -e545 -e725 */
    evt.status = event_unpack_uint16(&buffer); /* unifi_Status : status */
    /*lint -save -e545 -e725 */
    event_unpack_buffer(&buffer, (uint8*)&evt.stationMacAddress.data, 6); /*lint !e545 !e725  uint8 : data */

    unifi_sys_wifi_on_rsp(context, evt.status, &evt.stationMacAddress);

}

static void event_to_unifi_sys_wifi_off_req(FsmContext* context, uint8* inputbuffer, uint16 size)
{
    unifi_sys_wifi_off_req(context);

}

static void event_to_unifi_sys_wifi_off_rsp(FsmContext* context, uint8* inputbuffer, uint16 size)
{
    unifi_sys_wifi_off_rsp(context);

}

static void event_to_unifi_sys_suspend_rsp(FsmContext* context, uint8* inputbuffer, uint16 size)
{
    uint8* buffer = &inputbuffer[6];
    UnifiSysSuspendRsp_Evt evt;
    /*lint -save -e545 -e725 */
    evt.status = event_unpack_uint16(&buffer); /* unifi_Status : status */

    unifi_sys_suspend_rsp(context, evt.status);

}

static void event_to_unifi_sys_resume_rsp(FsmContext* context, uint8* inputbuffer, uint16 size)
{
    uint8* buffer = &inputbuffer[6];
    UnifiSysResumeRsp_Evt evt;
    /*lint -save -e545 -e725 */
    evt.status = event_unpack_uint16(&buffer); /* unifi_Status : status */

    unifi_sys_resume_rsp(context, evt.status);

}

static void event_to_unifi_sys_hip_req(FsmContext* context, uint8* inputbuffer, uint16 size)
{
    uint8* buffer = &inputbuffer[6];
    UnifiSysHipReq_Evt evt;
    evt.mlmeCommand.length = event_unpack_uint16(&buffer); /*lint !e725 unifi_DataBlock : mlmeCommand */
    evt.mlmeCommand.data = NULL;
    if (evt.mlmeCommand.length)
    {
        evt.mlmeCommand.data = (uint8*)osa_malloc(evt.mlmeCommand.length); /*lint !e725 unifi_DataBlock : mlmeCommand*/
        /*lint -save -e545 -e725 */
        event_unpack_buffer(&buffer, evt.mlmeCommand.data, evt.mlmeCommand.length); /*lint !e545 !e725 unifi_DataBlock : mlmeCommand */
    }
    evt.dataRef1.length = event_unpack_uint16(&buffer); /*lint !e725 unifi_DataBlock : dataRef1 */
    evt.dataRef1.data = NULL;
    if (evt.dataRef1.length)
    {
        evt.dataRef1.data = (uint8*)osa_malloc(evt.dataRef1.length); /*lint !e725 unifi_DataBlock : dataRef1*/
        /*lint -save -e545 -e725 */
        event_unpack_buffer(&buffer, evt.dataRef1.data, evt.dataRef1.length); /*lint !e545 !e725 unifi_DataBlock : dataRef1 */
    }
    evt.dataRef2.length = event_unpack_uint16(&buffer); /*lint !e725 unifi_DataBlock : dataRef2 */
    evt.dataRef2.data = NULL;
    if (evt.dataRef2.length)
    {
        evt.dataRef2.data = (uint8*)osa_malloc(evt.dataRef2.length); /*lint !e725 unifi_DataBlock : dataRef2*/
        /*lint -save -e545 -e725 */
        event_unpack_buffer(&buffer, evt.dataRef2.data, evt.dataRef2.length); /*lint !e545 !e725 unifi_DataBlock : dataRef2 */
    }

    unifi_sys_hip_req(context, &evt.mlmeCommand, &evt.dataRef1, &evt.dataRef2);

    if (evt.mlmeCommand.length)
    {
        osa_free(evt.mlmeCommand.data); /*lint !e725 unifi_DataBlock : mlmeCommand*/
    }
    if (evt.dataRef1.length)
    {
        osa_free(evt.dataRef1.data); /*lint !e725 unifi_DataBlock : dataRef1*/
    }
    if (evt.dataRef2.length)
    {
        osa_free(evt.dataRef2.data); /*lint !e725 unifi_DataBlock : dataRef2*/
    }
}

static void event_to_unifi_sys_qos_control_req(FsmContext* context, uint8* inputbuffer, uint16 size)
{
    uint8* buffer = &inputbuffer[6];
    UnifiSysQosControlReq_Evt evt;
    /*lint -save -e545 -e725 */
    evt.control = event_unpack_uint16(&buffer); /* unifi_QoSControl : control */

    unifi_sys_qos_control_req(context, evt.control);

}

static void event_to_unifi_sys_port_configure_req(FsmContext* context, uint8* inputbuffer, uint16 size)
{
    uint8* buffer = &inputbuffer[6];
    UnifiSysPortConfigureReq_Evt evt;
    /*lint -save -e545 -e725 */
    evt.uncontrolledPortAction = event_unpack_uint16(&buffer); /* unifi_PortAction : uncontrolledPortAction */
    /*lint -save -e545 -e725 */
    evt.controlledPortAction = event_unpack_uint16(&buffer); /* unifi_PortAction : controlledPortAction */
    /*lint -save -e545 -e725 */
    event_unpack_buffer(&buffer, (uint8*)&evt.macAddress.data, 6); /*lint !e545 !e725  uint8 : data */

    unifi_sys_port_configure_req(context, evt.uncontrolledPortAction, evt.controlledPortAction, &evt.macAddress);

}

static void event_to_unifi_sys_configure_power_mode_req(FsmContext* context, uint8* inputbuffer, uint16 size)
{
    uint8* buffer = &inputbuffer[6];
    UnifiSysConfigurePowerModeReq_Evt evt;
    /*lint -save -e545 -e725 */
    evt.mode = event_unpack_uint16(&buffer); /* unifi_LowPowerMode : mode */
    /*lint -save -e545 -e725 */
    evt.wakeHost = event_unpack_int16(&buffer); /* Boolean : wakeHost */

    unifi_sys_configure_power_mode_req(context, evt.mode, evt.wakeHost);

}

static void event_to_unifi_sys_traffic_config_req(FsmContext* context, uint8* inputbuffer, uint16 size)
{
    uint8* buffer = &inputbuffer[6];
    UnifiSysTrafficConfigReq_Evt evt;
    /*lint -save -e545 -e725 */
    evt.type = event_unpack_uint16(&buffer); /* unifi_traffic_configtype : type */
    /*lint -save -e545 -e725 */
    evt.config.packetFilter = event_unpack_uint16(&buffer); /* uint16 : packetFilter */
    /*lint -save -e545 -e725 */
    evt.config.customFilter.etherType = event_unpack_uint32(&buffer); /* uint32 : etherType */
    /*lint -save -e545 -e725 */
    evt.config.customFilter.ipType = event_unpack_uint8(&buffer); /* uint8 : ipType */
    /*lint -save -e545 -e725 */
    evt.config.customFilter.udpSourcePort = event_unpack_uint32(&buffer); /* uint32 : udpSourcePort */
    /*lint -save -e545 -e725 */
    evt.config.customFilter.udpDestPort = event_unpack_uint32(&buffer); /* uint32 : udpDestPort */

    unifi_sys_traffic_config_req(context, evt.type, &evt.config);

}

static void event_to_unifi_sys_media_status_req(FsmContext* context, uint8* inputbuffer, uint16 size)
{
    uint8* buffer = &inputbuffer[6];
    UnifiSysMediaStatusReq_Evt evt;
    /*lint -save -e545 -e725 */
    evt.mediaStatus = event_unpack_uint16(&buffer); /* unifi_MediaStatus : mediaStatus */

    unifi_sys_media_status_req(context, evt.mediaStatus);

}

static void event_to_unifi_sys_multicast_address_rsp(FsmContext* context, uint8* inputbuffer, uint16 size)
{
    uint8* buffer = &inputbuffer[6];
    UnifiSysMulticastAddressRsp_Evt evt;
    /*lint -save -e545 -e725 */
    evt.status = event_unpack_uint16(&buffer); /* unifi_Status : status */
    /*lint -save -e545 -e725 */
    evt.action = event_unpack_uint16(&buffer); /* unifi_ListAction : action */
    /*lint -save -e545 -e725 */
    evt.getAddresses.numElements = event_unpack_uint8(&buffer); /* uint8 : numElements */
    evt.getAddresses.addresses = NULL;
    if (evt.getAddresses.numElements)
    {
        int i;
        evt.getAddresses.addresses = (unifi_MACAddress*)osa_malloc(sizeof(unifi_MACAddress) * evt.getAddresses.numElements);
        for (i=0; i < evt.getAddresses.numElements; i++)
        {
            /*lint -save -e545 -e725 */
            event_unpack_buffer(&buffer, (uint8*)&evt.getAddresses.addresses[i].data, 6); /*lint !e545 !e725  uint8 : data */
        }
    }

    unifi_sys_multicast_address_rsp(context, evt.status, evt.action, &evt.getAddresses);

    if (evt.getAddresses.numElements)
    {
        int i;
        for (i=0; i < evt.getAddresses.numElements; i++)
        {
        }
        osa_free(evt.getAddresses.addresses);
    }
}

static void event_to_unifi_sys_tclas_add_req(FsmContext* context, uint8* inputbuffer, uint16 size)
{
    uint8* buffer = &inputbuffer[6];
    UnifiSysTclasAddReq_Evt evt;
    evt.tclas.length = event_unpack_uint16(&buffer); /*lint !e725 unifi_DataBlock : tclas */
    evt.tclas.data = NULL;
    if (evt.tclas.length)
    {
        evt.tclas.data = (uint8*)osa_malloc(evt.tclas.length); /*lint !e725 unifi_DataBlock : tclas*/
        /*lint -save -e545 -e725 */
        event_unpack_buffer(&buffer, evt.tclas.data, evt.tclas.length); /*lint !e545 !e725 unifi_DataBlock : tclas */
    }

    unifi_sys_tclas_add_req(context, &evt.tclas);

    if (evt.tclas.length)
    {
        osa_free(evt.tclas.data); /*lint !e725 unifi_DataBlock : tclas*/
    }
}

static void event_to_unifi_sys_tclas_del_req(FsmContext* context, uint8* inputbuffer, uint16 size)
{
    uint8* buffer = &inputbuffer[6];
    UnifiSysTclasDelReq_Evt evt;
    evt.tclas.length = event_unpack_uint16(&buffer); /*lint !e725 unifi_DataBlock : tclas */
    evt.tclas.data = NULL;
    if (evt.tclas.length)
    {
        evt.tclas.data = (uint8*)osa_malloc(evt.tclas.length); /*lint !e725 unifi_DataBlock : tclas*/
        /*lint -save -e545 -e725 */
        event_unpack_buffer(&buffer, evt.tclas.data, evt.tclas.length); /*lint !e545 !e725 unifi_DataBlock : tclas */
    }

    unifi_sys_tclas_del_req(context, &evt.tclas);

    if (evt.tclas.length)
    {
        osa_free(evt.tclas.data); /*lint !e725 unifi_DataBlock : tclas*/
    }
}

static void event_to_unifi_sys_traffic_classification_req(FsmContext* context, uint8* inputbuffer, uint16 size)
{
    uint8* buffer = &inputbuffer[6];
    UnifiSysTrafficClassificationReq_Evt evt;
    /*lint -save -e545 -e725 */
    evt.type = event_unpack_uint16(&buffer); /* unifi_traffic_type : type */
    /*lint -save -e545 -e725 */
    evt.period = event_unpack_uint16(&buffer); /* uint16 : period */

    unifi_sys_traffic_classification_req(context, evt.type, evt.period);

}

typedef void (*event_to_fn)(FsmContext* context, uint8* inputbuffer, uint16 size);
static const event_to_fn fnlookup[31] = {
        event_to_unifi_sys_wifi_on_req,
        NULL,
        event_to_unifi_sys_wifi_on_rsp,
        NULL,
        event_to_unifi_sys_wifi_off_req,
        NULL,
        event_to_unifi_sys_wifi_off_rsp,
        NULL,
        NULL,
        event_to_unifi_sys_suspend_rsp,
        NULL,
        event_to_unifi_sys_resume_rsp,
        event_to_unifi_sys_hip_req,
        NULL,
        event_to_unifi_sys_qos_control_req,
        NULL,
        event_to_unifi_sys_port_configure_req,
        NULL,
        event_to_unifi_sys_configure_power_mode_req,
        event_to_unifi_sys_traffic_config_req,
        NULL,
        NULL,
        NULL,
        event_to_unifi_sys_media_status_req,
        NULL,
        event_to_unifi_sys_multicast_address_rsp,
        event_to_unifi_sys_tclas_add_req,
        NULL,
        event_to_unifi_sys_tclas_del_req,
        NULL,
        event_to_unifi_sys_traffic_classification_req,
};

Boolean remote_sys_signal_receive(FsmContext* context, uint8* buffer, uint16 size)
{
    uint8* tempbuffer = buffer;
    uint16 id = event_unpack_uint16(&tempbuffer);
    uint16 idx = id - 0x8001;

    if (idx < 31)
    {
         if (fnlookup[idx])
         {
           (*fnlookup[idx])(context, buffer, size);
           return TRUE;
         }
    }
    return FALSE;
}

