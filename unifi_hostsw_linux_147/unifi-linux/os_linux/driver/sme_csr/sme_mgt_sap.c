/*
 * ---------------------------------------------------------------------------
 *
 * FILE: sme_mgt_sap.c
 *
 * PURPOSE:
 * This file provides the SME MGT API implementation.
 *
 * Copyright (C) 2008 Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * ---------------------------------------------------------------------------
 */

#include "unifi_priv.h"
#include "sme_top_level_fsm/sme.h"
#include "mgt_sap/mgt_sap_build_functions.h"


void unifi_mgt_wifi_on_req(FsmContext* context, const unifi_MACAddress* address, const unifi_DataBlockList* mibFiles)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_mgt_wifi_on_req()\n");

    buflen = build_packed_unifi_mgt_wifi_on_req(&buf, address, mibFiles);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}

void unifi_mgt_wifi_off_req(FsmContext* context)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_mgt_wifi_off_req\n");

    buflen = build_packed_unifi_mgt_wifi_off_req(&buf);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}

void unifi_mgt_set_value_req(FsmContext* context,
                             const unifi_AppValue* appValue)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_mgt_set_value_req(%d)\n", appValue->id);

    buflen = build_packed_unifi_mgt_set_value_req(&buf, appValue);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}

void unifi_mgt_get_value_req(FsmContext* context, unifi_AppValueId appValueId)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_mgt_get_value_req(%d)\n", appValueId);

    buflen = build_packed_unifi_mgt_get_value_req(&buf, appValueId);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}

void unifi_mgt_mib_get_req(FsmContext* context,
                           const unifi_DataBlock* mibAttribute)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_mgt_mib_get_req\n");

    buflen = build_packed_unifi_mgt_mib_get_req(&buf, mibAttribute);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}

void unifi_mgt_mib_set_req(FsmContext* context,
                           const unifi_DataBlock* mibAttribute)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_mgt_mib_set_req\n");

    buflen = build_packed_unifi_mgt_mib_set_req(&buf, mibAttribute);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}

void unifi_mgt_mib_get_next_req(FsmContext* context,
                                const unifi_DataBlock* mibAttribute)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_mgt_mib_get_next_req\n");

    buflen = build_packed_unifi_mgt_mib_get_next_req(&buf, mibAttribute);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}

void unifi_mgt_scan_full_req(FsmContext* context,
                             const unifi_SSID* ssid,
                             Boolean forceScan,
                             unifi_ScanType scanType,
                             const unifi_DataBlock* channelList)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_mgt_scan_full_req\n");

    buflen = build_packed_unifi_mgt_scan_full_req(&buf, ssid, forceScan, scanType, channelList);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}

void unifi_mgt_scan_results_get_req(FsmContext* context)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_mgt_scan_results_get_req\n");

    buflen = build_packed_unifi_mgt_scan_results_get_req(&buf);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}

void unifi_mgt_connect_req(FsmContext* context,
                           const unifi_ConnectionConfig* connectionConfig)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_mgt_connect_req\n");

    buflen = build_packed_unifi_mgt_connect_req(&buf, connectionConfig);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}

void unifi_mgt_disconnect_req(FsmContext* context)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_mgt_disconnect_req\n");

    buflen = build_packed_unifi_mgt_disconnect_req(&buf);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}

void unifi_mgt_multicast_address_req(FsmContext* context,
                                     unifi_ListAction action,
                                     const unifi_MulticastAddressList* addresses)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_mgt_multicast_address_req\n");

    buflen = build_packed_unifi_mgt_multicast_address_req(&buf, action,
                                                          addresses);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}

void unifi_mgt_pmkid_req(FsmContext* context, unifi_ListAction action,
                         const unifi_PmkidList* pmkids)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_mgt_pmkid_req\n");

    buflen = build_packed_unifi_mgt_pmkid_req(&buf, action, pmkids);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}

void unifi_mgt_key_req(FsmContext* context, unifi_ListAction action,
                       const unifi_Key* key)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_mgt_key_req\n");

    buflen = build_packed_unifi_mgt_key_req(&buf, action, key);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}

void unifi_mgt_packet_filter_set_req(FsmContext* context,
                                     const unifi_DataBlock* filter,
                                     unifi_PacketFilterMode mode,
                                     unifi_IPV4Address ipV4Address)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_mgt_packet_filter_set_req\n");

    buflen = build_packed_unifi_mgt_packet_filter_set_req(&buf, filter,
                                                          mode, ipV4Address);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}

void unifi_mgt_tspec_req(FsmContext* context,
                         unifi_ListAction action,
                         uint32 transactionId,
                         Boolean strict,
                         const unifi_DataBlock* tspec,
                         const unifi_DataBlock* tclas)
{
    unifi_priv_t* priv = (unifi_priv_t*)context;
    uint8* buf;
    uint16 buflen;

    unifi_trace(priv, UDBG2, "unifi_mgt_tspec_req\n");

    buflen = build_packed_unifi_mgt_tspec_req(&buf, action, transactionId,
                                              strict, tspec, tclas);

    /* Send message to user space */
    sme_queue_message(priv, buf, buflen);
}

