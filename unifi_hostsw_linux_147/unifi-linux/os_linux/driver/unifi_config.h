/*
 * ---------------------------------------------------------------------------
 *
 * FILE: unifi_config.h
 * 
 * PURPOSE:
 *      This header file provides parameters that configure the operation
 *      of the driver.
 *
 * Copyright (C) 2006-2008 by Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * ---------------------------------------------------------------------------
 */
#ifndef __UNIFI_CONFIG_H__
#define __UNIFI_CONFIG_H__ 1


/* 
 * It is sometimes useful to force all bulk data transfers to be a multiple
 * of the SDIO block size, so the SDIO driver won't try to use
 * a byte-mode CMD53. These are broken on some hardware platforms.
 * 
 * Define UNIFI_PAD_BULK_DATA_TO_BLOCK_SIZE to force it.
 */
#define UNIFI_PAD_BULK_DATA_TO_BLOCK_SIZE


/* 
 * It is sometimes useful to force all signal transfers to be a multiple
 * of the SDIO block size, so the SDIO driver won't try to use
 * a byte-mode CMD53. These are broken on some hardware platforms.
 * 
 * Define UNIFI_PAD_SIGNALS_TO_BLOCK_SIZE to force it.
 */
#define UNIFI_PAD_SIGNALS_TO_BLOCK_SIZE


#endif /* __UNIFI_CONFIG_H__ */
