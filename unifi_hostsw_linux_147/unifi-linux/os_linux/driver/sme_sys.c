/*
 * ---------------------------------------------------------------------------
 * FILE:     sme_sys.c
 *
 * PURPOSE:
 *      Driver specific implementation of the SME SYS SAP.
 *
 * Copyright (C) 2008 by Cambridge Silicon Radio Ltd.
 *
 * Refer to LICENSE.txt included with this source code for details on
 * the license terms.
 *
 * ---------------------------------------------------------------------------
 */

#include "driver/unifiversion.h"
#include "unifi_priv.h"


/*
 * This file implements the SME SYS API. It contains the following functions:
 * unifi_sys_media_status_req()
 * unifi_sys_hip_req()
 * unifi_sys_port_configure_req()
 * unifi_sys_wifi_on_req()
 * unifi_sys_wifi_off_req()
 * unifi_sys_suspend_rsp()
 * unifi_sys_resume_rsp()
 * unifi_sys_qos_control_req()
 * unifi_sys_configure_power_mode_req()
 * unifi_sys_wifi_on_rsp()
 * unifi_sys_wifi_off_rsp()
 * unifi_sys_multicast_address_rsp()
 * unifi_sys_traffic_config_req()
 * unifi_sys_traffic_classification_req()
 * unifi_sys_tclas_add_req()
 * unifi_sys_tclas_del_req()
 */

/*
 * Handling the suspend and resume system events, we need to block
 * until the SME sends the response to our indication.
 * We use the sme_init_request() and sme_wait_for_reply()
 * to implement this behavior in the following functions:
 * sme_sys_suspend()
 * sme_sys_resume()
 */


void unifi_sys_media_status_req(void* drvpriv, unifi_MediaStatus mediaStatus)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv->smepriv == NULL) {
        unifi_error(priv, "unifi_sys_media_status_req: invalid smepriv\n");
        return;
    }

    if (mediaStatus == unifi_MediaConnected) {
        unifi_sys_ip_configured_ind(priv->smepriv, (priv->sta_ip_address != 0xFFFFFFFF));

        netif_carrier_on(priv->netdev);
        UF_NETIF_TX_WAKE_ALL_QUEUES(priv->netdev);

        priv->connected = UnifiConnected;
    } else  {
        netif_carrier_off(priv->netdev);
        priv->connected = UnifiNotConnected;
    }
}


void unifi_sys_hip_req(void* drvpriv, const unifi_DataBlock* mlmeCommand,
                       const unifi_DataBlock* dataRef1,
                       const unifi_DataBlock* dataRef2)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;
    bulk_data_param_t bulkdata;
    u8 *signal_ptr;
    int signal_length;
    int r;
    void *dest;

    if (priv == NULL) {
        return;
    }

    if (priv->smepriv == NULL) {
        unifi_error(priv, "unifi_sys_hip_req: invalid smepriv\n");
        return;
    }

    unifi_trace(priv, UDBG4, "unifi_sys_hip_req: 0x04%X ---->\n",
                *((uint16*)mlmeCommand->data));

    /* Construct the signal. */
    signal_ptr = (u8*)mlmeCommand->data;
    signal_length = mlmeCommand->length;

    /*
     * The MSB of the sender ID needs to be set to the client ID.
     * The LSB is controlled by the SME.
     */
    signal_ptr[5] = (priv->sme_cli->sender_id >> 8) & 0xff;

    /* Allocate buffers for the bulk data. */
    if (dataRef1->length) {
        r = unifi_net_data_malloc(priv, &bulkdata.d[0], dataRef1->length);
        if (r == 0) {
            dest = (void*)bulkdata.d[0].os_data_ptr;
            memcpy(dest, dataRef1->data, dataRef1->length);
            bulkdata.d[0].data_length = dataRef1->length;
        } else {
        }
    } else {
        bulkdata.d[0].data_length = 0;
    }
    if (dataRef2->length) {
        r = unifi_net_data_malloc(priv, &bulkdata.d[1], dataRef2->length);
        if (r == 0) {
            dest = (void*)bulkdata.d[1].os_data_ptr;
            memcpy(dest, dataRef2->data, dataRef2->length);
            bulkdata.d[1].data_length = dataRef2->length;
        } else {
        }
    } else {
        bulkdata.d[1].data_length = 0;
    }

    unifi_trace(priv, UDBG3, "SME SEND: Signal %s \n",
                lookup_signal_name(*((uint16*)signal_ptr)));

    r = ul_send_signal_raw(priv, (const unsigned char*)signal_ptr,
                           signal_length, &bulkdata);
    if (r) {
        unifi_error(priv, "unifi_sys_hip_req: Failed to send signal\n");
        /* FIXME: If r==NOCARD then ind unifi_Control_Exit */
        unifi_sys_wifi_off_ind(priv->smepriv, unifi_Control_Error);
    }

    unifi_trace(priv, UDBG4, "unifi_sys_hip_req: <----\n");
}




/*
 * ---------------------------------------------------------------------------
 * configure_controlled_port
 *
 *      Store the new controlled port configuration.
 *
 * Arguments:
 *      priv            Pointer to device private context struct
 *      port_cfg        Pointer to the controlled port configuration
 *
 * Returns:
 *      An unifi_ControlledPortAction value.
 * ---------------------------------------------------------------------------
 */
static int
configure_controlled_port(unifi_priv_t *priv, const unifi_controlled_port_cfg *port_cfg)
{
    const unsigned char broadcast_mac_address[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

    /*
     * If the new configuration has the broadcast MAC address
     * we invalidate all entries in the list and store the new
     * configuration in index 0.
     */
    if (memcmp(port_cfg->mac_address, broadcast_mac_address, ETH_ALEN) == 0) {

        memcpy((void*)(&priv->controlled_port_cfg.port_cfg[0]),
               (void*)port_cfg, sizeof(unifi_controlled_port_cfg));
        priv->controlled_port_cfg.entries_in_use = 1;
        priv->controlled_port_cfg.overide_action = CONTROLLED_PORT_OVERIDE;

        if (port_cfg->port_action == unifi_8021x_PortOpen) {
            unifi_trace(priv, UDBG1, "Controlled port broadcast set to open.\n");

            /*
             * Ask stack to schedule for transmission any packets queued
             * while controlled port was not open.
             * Use netif_schedule() instead of netif_wake_queue() because
             * transmission should be already enabled at this point. If it
             * is not, probably the interface is down and should remain as is.
             */
            uf_resume_xmit(priv);
        } else {
            unifi_trace(priv, UDBG1, "Controlled port broadcast set to %s.\n",
                        (port_cfg->port_action == unifi_8021x_PortClosedDiscard) ? "discard": "closed");
        }
    } else {
        /*
         * If the new configuration is for Ad-Hoc we clear the overide action,
         * find the first available index and store the new configuration.
         */
        if (priv->controlled_port_cfg.entries_in_use < UNIFI_MAX_CONNECTIONS) {
            unifi_controlled_port_cfg *next_port_cfg;
            /* If the broadcast configuration is still set, reset it. */
            if (priv->controlled_port_cfg.overide_action == CONTROLLED_PORT_OVERIDE) {
                priv->controlled_port_cfg.entries_in_use = 0;
                priv->controlled_port_cfg.overide_action = CONTROLLED_PORT_NOT_OVERIDE;
            }
            /* Use the next available index. */
            next_port_cfg = &priv->controlled_port_cfg.port_cfg[priv->controlled_port_cfg.entries_in_use];
            memcpy((void*)next_port_cfg, (void*)port_cfg,
                   sizeof(unifi_controlled_port_cfg));
            priv->controlled_port_cfg.entries_in_use ++;

            if (next_port_cfg->port_action == unifi_8021x_PortOpen) {
                /*
                 * Ask stack to schedule for transmission any packets queued
                 * while controlled port was not open.
                 * Use netif_schedule() instead of netif_wake_queue() because
                 * transmission should be already enabled at this point. If it
                 * is not, probably the interface is down and should remain as is.
                 */
                uf_resume_xmit(priv);
            }

            unifi_trace(priv, UDBG1, "add a new port config (%d).\n", port_cfg->port_action);
        } else {
            unifi_error(priv, "controlled_port_cfg is full.\n");
            return -EFAULT;
        }
    }
    return 0;
} /* configure_controlled_port() */


void unifi_sys_port_configure_req(void* drvpriv,
                                  unifi_PortAction uncontrolledPortAction,
                                  unifi_PortAction controlledPortAction,
                                  const unifi_MACAddress* macAddress)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;
    unifi_controlled_port_cfg controlled_port;

    if (priv->smepriv == NULL) {
        unifi_error(priv, "unifi_sys_port_configure_req: invalid smepriv\n");
        return;
    }

    memset(&controlled_port, 0, sizeof(unifi_controlled_port_cfg));
    controlled_port.port_action = controlledPortAction;
    memcpy(controlled_port.mac_address, macAddress->data, 6);

    configure_controlled_port(priv, &controlled_port);

    unifi_sys_port_configure_cfm(priv->smepriv, unifi_Success, macAddress);
}


void unifi_sys_wifi_on_req(void* drvpriv)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;
    unifi_Versions versions;
    int r;

    if (priv == NULL) {
        return;
    }

    unifi_trace(priv, UDBG1, "unifi_sys_wifi_on_req\n");

    /*
     * The request to initialise UniFi might come while UniFi is running.
     * We need to block all I/O activity until the reset completes, otherwise
     * an SDIO error might occur resulting an indication to the SME which
     * makes it think that the initialisation has failed.
     */
    priv->bh_thread.block_thread = 1;

    /* Update the wifi_on state */
    priv->wifi_on_state = wifi_on_in_progress;

    r = uf_request_firmware_files(priv);
    if (r) {
        unifi_error(priv, "unifi_sys_wifi_on_req: Failed to get f/w\n");
        unifi_sys_wifi_on_cfm(priv->smepriv, unifi_Error);
        return;
    }

    /* Power on UniFi */
    r = unifi_sdio_power_on(priv->sdio);
    if (r < 0) {
        unifi_error(priv, "unifi_sys_wifi_on_req: Failed to power on UniFi\n");
        unifi_sys_wifi_on_cfm(priv->smepriv, unifi_Error);
        return;
    }

    /* If unifi_sdio_power_on() returns 0, it means that we need to initialise UniFi */
    if (r == 0) {
        /* Initialise UniFi hardware */
        r = uf_init_hw(priv);
        if (r) {
            unifi_error(priv, "unifi_sys_wifi_on_req: Failed to initialise h/w, error %d\n", r);
            unifi_sys_wifi_on_cfm(priv->smepriv, unifi_Error);
            return;
        }
    }

    /* Start the I/O thread */
    uf_init_bh(priv);

    /* Get the version information from the core */
    unifi_card_info(priv->card, &priv->card_info);

    /* Copy to the unifiio_card_info structure. */
    versions.chipId = priv->card_info.chip_id;
    versions.chipVersion = priv->card_info.chip_version;
    versions.firmwareBuild = priv->card_info.fw_build;
    versions.firmwareHip = priv->card_info.fw_hip_version;
    versions.driverBuild = UNIFI_DRIVER_BUILD_ID;
    versions.driverHip = (UNIFI_HIP_MAJOR_VERSION << 8) | UNIFI_HIP_MINOR_VERSION;
    versions.smeDriverApi = (SME_API_MAJOR_VERSION << 8) | SME_API_MINOR_VERSION;
    versions.smeIdString = "";

    unifi_sys_wifi_on_ind(priv->smepriv, unifi_Success, &versions);

    /* Update the wifi_on state */
    priv->wifi_on_state = wifi_on_done;
}


/*
 * wifi_off:
 *      Common code for unifi_sys_wifi_off_req() and
 *      unifi_sys_wifi_off_rsp().
 */
static void
wifi_off(unifi_priv_t *priv)
{
    int power_off;
    int priv_instance;

    /* Destroy the Traffic Analysis Module */
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,23)
    cancel_work_sync(&priv->ta_ind_work.task);
    cancel_work_sync(&priv->ta_sample_ind_work.task);
#ifdef CSR_SUPPORT_WEXT
    cancel_work_sync(&priv->sme_config_task);
#endif
#endif
    flush_workqueue(priv->unifi_workqueue);

    /* fw_init parameter can prevent power off UniFi, for debugging */
    priv_instance = unifi_find_priv(priv);
    if (priv_instance == -1) {
        unifi_warning(priv,
                      "unifi_sys_stop_req: Unknown priv instance, will power off card.\n");
        power_off = 1;
    } else {
        power_off = (fw_init[priv_instance] > 0) ? 0 : 1;
    }

    /* Stop the bh_thread */
    uf_stop_thread(priv, &priv->bh_thread);

    if (power_off) {
        unifi_trace(priv, UDBG2,
                    "Force low power and try to power off\n");
        /* Put UniFi to deep sleep, in case we can not power it off */
        uf_sdio_claim(priv->sdio);
        unifi_force_low_power_mode(priv->card);
        uf_sdio_release(priv->sdio);

        unifi_sdio_power_off(priv->sdio);
    }

    /* Consider UniFi to be uninitialised */
    priv->init_progress = UNIFI_INIT_NONE;

} /* wifi_off() */


void unifi_sys_wifi_off_req(void* drvpriv)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        return;
    }

    unifi_trace(priv, UDBG1, "unifi_sys_wifi_off_req:\n");


    /* Stop the network traffic before freeing the core. */
    if (priv->netdev_registered == 1) {
        netif_carrier_off(priv->netdev);
        UF_NETIF_TX_STOP_ALL_QUEUES(priv->netdev);
    }

    wifi_off(priv);

    unifi_sys_wifi_off_cfm(priv->smepriv);
}


void unifi_sys_qos_control_req(void* drvpriv, unifi_QoSControl control)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv->smepriv == NULL) {
        unifi_error(priv, "unifi_sys_qos_control_req: invalid smepriv\n");
        return;
    }

    unifi_trace(priv, UDBG4,
                "unifi_sys_qos_control_req: control = %d", control);

    if (control == unifi_QoSWMMOn) {
        priv->sta_wmm_capabilities |= QOS_CAPABILITY_WMM_ENABLED;
    } else {
        priv->sta_wmm_capabilities = 0;
    }

    unifi_sys_qos_control_cfm(priv->smepriv, unifi_Success);
}


void unifi_sys_tclas_add_req(void* drvpriv, const unifi_DataBlock* tclas)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(priv, "unifi_sys_tclas_add_req: invalid smepriv\n");
        return;
    }

    unifi_sys_tclas_add_cfm(priv->smepriv, unifi_Success);
}


void unifi_sys_tclas_del_req(void* drvpriv, const unifi_DataBlock* tclas)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(priv, "unifi_sys_tclas_del_req: invalid smepriv\n");
        return;
    }

    unifi_sys_tclas_del_cfm(priv->smepriv, unifi_Success);
}


void unifi_sys_configure_power_mode_req(void* drvpriv, unifi_LowPowerMode mode, Boolean wakeHost)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;
    enum unifi_low_power_mode pm;

    if (priv->smepriv == NULL) {
        unifi_error(priv, "unifi_sys_hw_check_req: invalid smepriv\n");
        return;
    }

    if (mode == unifi_LowPowerDisabled) {
        pm = UNIFI_LOW_POWER_DISABLED;
    } else {
        pm = UNIFI_LOW_POWER_ENABLED;
    }

    unifi_trace(priv, UDBG2,
                "unifi_sys_configure_power_mode_req (mode=%d, wake=%d)\n",
                mode, wakeHost);
    unifi_configure_low_power_mode(priv->card, pm,
                                   (wakeHost ? UNIFI_PERIODIC_WAKE_HOST_ENABLED : UNIFI_PERIODIC_WAKE_HOST_DISABLED));
}


void unifi_sys_wifi_on_rsp(void* drvpriv, unifi_Status status,
                           const unifi_MACAddress *stationMacAddress)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(NULL, "unifi_sys_wifi_on_rsp: Invalid ospriv.\n");
        return;
    }

    unifi_trace(priv, UDBG1,
                "unifi_sys_wifi_on_rsp: status %d\n", status);

    /* UniFi is now initialised, complete the init. */
    if (status == unifi_Success)
    {
        /* Register the UniFi device with the OS network manager */
        unifi_trace(priv, UDBG3, "Card Init Completed Successfully\n");

        /* Store the MAC address in the netdev */
        memcpy(priv->netdev->dev_addr, stationMacAddress->data, ETH_ALEN);

        if (!priv->netdev_registered)
        {
            int r;
            unifi_trace(priv, UDBG1, "registering net device\n");
            r = uf_register_netdev(priv);
            if (r) {
                unifi_error(priv, "Failed to register the network device.\n");
                unifi_sys_wifi_on_cfm(priv->smepriv, unifi_Error);
                return;
            }
        }

        priv->init_progress = UNIFI_INIT_COMPLETED;

        /* Acknowledge the unifi_sys_wifi_on_req() now */
        unifi_sys_wifi_on_cfm(priv->smepriv, unifi_Success);

        unifi_info(priv, "UniFi ready\n");

#ifdef CSR_SUPPORT_WEXT
        queue_work(priv->unifi_workqueue, &priv->sme_config_task);
#endif

    } else {
        /* Acknowledge the unifi_sys_wifi_on_req() now */
        unifi_sys_wifi_on_cfm(priv->smepriv, unifi_Error);
    }
}


void unifi_sys_wifi_off_rsp(void* drvpriv)
{
}


void unifi_sys_multicast_address_rsp(void* drvpriv, unifi_Status status,
                                     unifi_ListAction action,
                                     const unifi_MulticastAddressList* addresses)
{
}


void unifi_sys_suspend_rsp(void* drvpriv, unifi_Status status)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(priv, "unifi_sys_suspend_rsp: invalid priv\n");
        return;
    }

    sme_complete_request(priv, status);
}


void unifi_sys_resume_rsp(void* drvpriv, unifi_Status status)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(priv, "unifi_sys_resume_rsp: invalid priv\n");
        return;
    }

    /*
     * Nothing is waiting for the response.
     * Do not call sme_complete_request(), otherwise the driver
     * and the SME will be out of step.
     */
}


int sme_sys_suspend(unifi_priv_t *priv)
{
    int r;

    if (priv->smepriv == NULL) {
        unifi_error(priv, "sme_sys_suspend: invalid smepriv\n");
        return -EIO;
    }

    r = sme_init_request(priv);
    if (r) {
        return -EIO;
    }

    unifi_sys_suspend_ind(priv->smepriv, 0, 0);
    r = sme_wait_for_reply(priv, UNIFI_SME_SYS_LONG_TIMEOUT);
    if (r) {
        unifi_notice(priv,
                     "suspend: SME did not reply, power off UniFi anyway\n");
    }

    /* Put UniFi to deep sleep, in case we can not power it off */
    uf_sdio_claim(priv->sdio);
    unifi_force_low_power_mode(priv->card);
    uf_sdio_release(priv->sdio);

    unifi_sdio_power_off(priv->sdio);

    /* Consider UniFi to be uninitialised */
    priv->init_progress = UNIFI_INIT_NONE;

    unifi_trace(priv, UDBG1, "sme_sys_suspend: <-- (%d)\n", r);
    return convert_sme_error(priv->sme_reply.reply_status);
}


int sme_sys_resume(unifi_priv_t *priv)
{
    int r;

    if (priv->smepriv == NULL) {
        unifi_error(priv, "sme_sys_resume: invalid smepriv\n");
        return -EIO;
    }

    r = sme_init_request(priv);
    if (r) {
        return -EIO;
    }

    unifi_sys_resume_ind(priv->smepriv, 0);

    /*
     * We are not going to wait for the reply because the SME might be in
     * the userspace. In this case the event will reach it when the kernel
     * resumes. So, release now the SME semaphore that was downed in
     * sme_init_request().
     */
    up(&priv->sme_sem);
    return 0;
}

void unifi_sys_traffic_config_req(void* drvpriv, unifi_traffic_configtype type, const unifi_traffic_config *config)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(priv, "unifi_sys_traffic_config_req: invalid smepriv\n");
        return;
    }

    unifi_ta_configure(priv->card, type, config);
}

void unifi_sys_traffic_classification_req(void* drvpriv, unifi_traffic_type traffic_type, uint16 period)
{
    unifi_priv_t *priv = (unifi_priv_t*)drvpriv;

    if (priv == NULL) {
        unifi_error(priv, "unifi_sys_traffic_classification_req: invalid smepriv\n");
        return;
    }

    unifi_ta_classification(priv->card, traffic_type, period);
}

