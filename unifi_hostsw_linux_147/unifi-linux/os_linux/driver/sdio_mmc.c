/*
 * ---------------------------------------------------------------------------
 *
 * FILE: sdio_mmc.c
 *
 * PURPOSE: SDIO driver interface for generic MMC stack.
 *
 * Copyright (C) 2008 by Cambridge Silicon Radio Ltd.
 *
 * ---------------------------------------------------------------------------
 */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/mutex.h>
#include <linux/gfp.h>

#include <linux/mmc/core.h>
#include <linux/mmc/card.h>
#include <linux/mmc/host.h>
#include <linux/mmc/sdio_func.h>
#include <linux/mmc/sdio_ids.h>
#include <linux/mmc/sdio.h>

#include "driver/unifi.h"
#include "unifi_priv.h"

#ifdef UNIFI_DEBUG

static int Claimed;
#define CHECK_CLAIM() do { if (!Claimed) printk("** sdio not claimed ***\n"); } while (0)

#define _sdio_claim_host(_func)                                         \
    do {                                                                \
        if (func->card->host->claimed) {                                \
            printk("%s: host already claimed, will wait\n", __FUNCTION__); \
        }                                                               \
        sdio_claim_host(_func);                                         \
        Claimed = 1;                                                    \
    } while (0)

#define _sdio_release_host(_func)                               \
    do {                                                        \
        if (!func->card->host->claimed) {                       \
            printk("%s: host not claimed\n", __FUNCTION__);     \
        }                                                       \
        sdio_release_host(_func);                               \
        Claimed = 0;                                            \
    } while (0)

#else

#define CHECK_CLAIM() {}
#define _sdio_claim_host(_func)  sdio_claim_host(_func)
#define _sdio_release_host(_func)  sdio_release_host(_func)

#endif


/* MMC uses ENOMEDIUM to indicate card gone away */
#define convert_sdio_error(_r) (((_r) == -ENOMEDIUM) ? -ENODEV : (_r))


void
uf_sdio_claim(void *sdio)
{
    struct sdio_func *func = sdio;

    _sdio_claim_host(func);
}

void
uf_sdio_release(void *sdio)
{
    struct sdio_func *func = sdio;

    _sdio_release_host(func);
}


int
unifi_sdio_readb(void *sdio, int funcnum, unsigned long addr,
                 unsigned char *pdata)
{
    struct sdio_func *func = sdio;
    int err = 0;

    CHECK_CLAIM();

    if (funcnum == 0) {
        *pdata = sdio_f0_readb(func, addr, &err);
    } else {
        *pdata = sdio_readb(func, addr, &err);
    }
    if (err) func_exit_r(err);

    return convert_sdio_error(err);
} /* unifi_sdio_readb() */

int
unifi_sdio_writeb(void *sdio, int funcnum, unsigned long addr,
                  unsigned char data)
{
    struct sdio_func *func = sdio;
    int err = 0;

    CHECK_CLAIM();

    if (funcnum == 0) {
        sdio_f0_writeb(func, data, addr, &err);
    } else {
        sdio_writeb(func, data, addr, &err);
    }
    if (err) func_exit_r(err);

    return convert_sdio_error(err);
} /* unifi_sdio_writeb() */


int
unifi_sdio_block_rw(void *sdio, int funcnum, unsigned long addr,
                    unsigned char *pdata, unsigned int count, int direction)
{
    struct sdio_func *func = sdio;
    int err = 0;

    CHECK_CLAIM();

    if (direction) {
        err = sdio_writesb(func, addr, pdata, count);
    } else {
        err = sdio_readsb(func, pdata, addr, count);
    }
    if (err) func_exit_r(err);

    return convert_sdio_error(err);
} /* unifi_sdio_block_rw() */


/*
 * ---------------------------------------------------------------------------
 *  unifi_sdio_set_max_clock_speed
 *
 *      Set the maximum SDIO bus clock speed to use.
 *
 *  Arguments:
 *      sdio            SDIO context pointer
 *      max_khz         maximum clock speed in kHz
 *
 *  Returns:
 *      Set clock speed in kHz; or a UniFi driver error code.
 * ---------------------------------------------------------------------------
 */
int
unifi_sdio_set_max_clock_speed(void *sdio, int max_khz)
{
    struct sdio_func *func = sdio;

    CHECK_CLAIM();

    if (max_khz <= 0 || max_khz > sdio_clock) {
        max_khz = sdio_clock;
    }
    sdio_set_clock(func, 1000 * max_khz);

    return max_khz;
} /* unifi_sdio_set_max_clock_speed() */


/*
 * ---------------------------------------------------------------------------
 *  unifi_sdio_enable_interrupt
 *
 *      Enable or disable the SDIO interrupt.
 *      The driver disables the SDIO interrupt until the i/o thread can
 *      process it.
 *      The SDIO interrupt can be disabled by modifying the SDIO_INT_ENABLE
 *      register in the Card Common Control Register block, but this requires
 *      two CMD52 operations. A better solution is to mask the interrupt at
 *      the host controller.
 * 
 *  Arguments:
 *      sdio            SDIO context pointer
 *      enable          If zero disable (or mask) the interrupt, otherwise
 *                      enable (or unmask) it.
 *
 *  Returns:
 *      Zero on success or a UniFi driver error code.
 *
 *  Notes:
 *      Called from unifi_bh() and unifi_sdio_interrupt_handler().
 *      The SDIO host is claimed by uf_sdio_claim() around unifi_bh() or
 *      by the MMC core interrupt processing.
 * ---------------------------------------------------------------------------
 */
int
unifi_sdio_enable_interrupt(void *sdio, int enable)
{
    struct sdio_func *func = sdio;
    int err = 0;

    func_enter();

    if (enable) {
        sdio_f0_writeb(func, 0x3, SDIO_CCCR_IENx, &err);
    } else {
        sdio_f0_writeb(func, 0, SDIO_CCCR_IENx, &err);
    }

    func_exit();
    return convert_sdio_error(err);
} /* unifi_sdio_enable_interrupt() */


/*
 * ---------------------------------------------------------------------------
 *  unifi_sdio_enable
 *
 *      Enable i/o on function 1.
 * 
 *  Arguments:
 *      sdio            SDIO context pointer
 *
 * Returns:
 *      UniFi driver error code.
 * ---------------------------------------------------------------------------
 */
int
unifi_sdio_enable(void *sdio)
{
    struct sdio_func *func = sdio;
    int err = 0;

    func_enter();

    /* Enable UniFi function 1 (the 802.11 part). */
    err = sdio_enable_func(func);
    if (err) {
        unifi_error(NULL, "Failed to enable SDIO function %d\n", func->num);
    }

    func_exit();
    return convert_sdio_error(err);
} /* unifi_sdio_enable() */


/*
 * ---------------------------------------------------------------------------
 *  unifi_sdio_disable
 *
 *      Enable i/o on function 1.
 * 
 *  Arguments:
 *      sdio            SDIO context pointer
 *
 * Returns:
 *      UniFi driver error code.
 * ---------------------------------------------------------------------------
 */
int
unifi_sdio_disable(void *sdio)
{
    struct sdio_func *func = sdio;
    int err = 0;

    func_enter();

    /* Enable UniFi function 1 (the 802.11 part). */
    err = sdio_disable_func(func);
    if (err) {
        unifi_error(NULL, "Failed to disable SDIO function %d\n", func->num);
    }

    func_exit();
    return convert_sdio_error(err);
} /* unifi_sdio_disable() */


/*
 * ---------------------------------------------------------------------------
 *  unifi_sdio_active
 *
 *      No-op as the bus goes to an active state at the start of every
 *      command.
 * 
 *  Arguments:
 *      sdio            SDIO context pointer
 * ---------------------------------------------------------------------------
 */
void
unifi_sdio_active(void *sdio)
{
    struct sdio_func *func = sdio;
    /* Restart SDIO bus clock */
    sdio_set_clock(func, 1000 * sdio_clock);
}

/*
 * ---------------------------------------------------------------------------
 *  unifi_sdio_idle
 *
 *      Set the function as idle.
 * 
 *  Arguments:
 *      sdio            SDIO context pointer
 * ---------------------------------------------------------------------------
 */
void
unifi_sdio_idle(void *sdio)
{
    struct sdio_func *func = sdio;

    sdio_set_clock(func, func->card->host->f_min);
} /* unifi_sdio_idle() */


/* 
 * Try using mmc_suspend_host() and mmc_resume_host() for power control
 * and h/w reset.
 * Note: Suspend and resume bus ops are not supported by the SDIO driver.
 */
int unifi_sdio_power_on(void *sdio)
{
#if 0
    struct sdio_func *func = sdio;
    sdio_resume_host(func);
#endif
    return 0; /* 0 means power was off, reinit required */
}

void unifi_sdio_power_off(void *sdio)
{
#if 0
    struct sdio_func *func = sdio;
    sdio_suspend_host(func, PMSG_SUSPEND);
#endif
}


/*
 * ---------------------------------------------------------------------------
 *  unifi_sdio_hard_reset
 *
 *      Hard Resets UniFi is possible.
 * 
 *  Arguments:
 *      sdio            SDIO context pointer
 *
 * Returns:
 * 1 if the SDIO driver is not capable of doing a hard reset.
 * 0 if a hard reset was successfully performed.
 * -EIO if an I/O error occured while re-initializing the
 * card.  This is a fatal, non-recoverable error.
 * -ENODEV if the card is no longer present.
 * ---------------------------------------------------------------------------
 */
int unifi_sdio_hard_reset(void *sdio)
{
    return 1;
} /* unifi_sdio_hard_reset() */


/*
 * ---------------------------------------------------------------------------
 *  unifi_sdio_get_info
 *
 *      Return UniFi information read by the SDIO driver.
 * 
 *  Arguments:
 *      sdio            SDIO context pointer
 *      param_type      The enum value for the required information
 *      param_value     Pointer to store the returned information
 *
 * Returns:
 *      UniFi driver error code.
 * ---------------------------------------------------------------------------
 */
int
unifi_sdio_get_info(void *sdio, enum unifi_sdio_request param_type, unsigned int *param_value)
{
    struct sdio_func *func = sdio;

    switch (param_type) {
      case UNIFI_SDIO_IO_BLOCK_SIZE:
        *param_value = func->cur_blksize;
        break;
      case UNIFI_SDIO_VENDOR_ID:
        *param_value = func->vendor;
        break;
      case UNIFI_SDIO_DEVICE_ID:
        *param_value = func->device;
        break;
      case UNIFI_SDIO_FUNCTION_NUM:
        *param_value = func->num;
        break;
      default:
        return -EINVAL;
    }

    return 0;

} /* unifi_sdio_get_info() */



/*
 * ---------------------------------------------------------------------------
 *  uf_sdio_int_handler
 *
 *      Interrupt callback function for SDIO interrupts.
 *      This is called in kernel context (i.e. not interrupt context).
 *      We retrieve the unifi context pointer and call the main UniFi
 *      interrupt handler.
 *
 *  Arguments:
 *      fdev      SDIO context pointer
 *
 *  Returns:
 *      None.
 *
 *  Note: Called with host already claimed.
 * ---------------------------------------------------------------------------
 */
static void
uf_sdio_int_handler(struct sdio_func *func)
{
    unifi_priv_t *priv;

    priv = sdio_get_drvdata(func);

    if (priv) {
        unifi_sdio_interrupt_handler(priv->card);
    }
} /* uf_sdio_int_handler() */



/*
 * ---------------------------------------------------------------------------
 *  uf_sdio_probe
 *
 *      Card insert callback.
 *
 * Arguments:
 *      fdev            SDIO context pointer
 *
 * Returns:
 *      UniFi driver error code.
 * ---------------------------------------------------------------------------
 */
static int
uf_sdio_probe(struct sdio_func *func,
              const struct sdio_device_id *id)
{
    void *priv;
    int instance;
    int r;

    func_enter();

    _sdio_claim_host(func);

    /* Assumes one card per host, which is true for SDIO */
    instance = func->card->host->index;
    unifi_info(NULL, "card 0x%X inserted\n", instance);

    /* Always override default SDIO bus clock */
    unifi_trace(NULL, UDBG1, "Setting SDIO bus clock to %d kHz\n", sdio_clock);
    sdio_set_clock(func, 1000 * sdio_clock);

    /* Hook up our interupt handler */
    r = sdio_claim_irq(func, uf_sdio_int_handler);
    if (r) {
        printk(KERN_ERR "Failed to claim SDIO interrupt: error %d\n", r);
    }

    printk("sdio bus_id: %16s\n", sdio_func_id(func));
    /* Register this device with the main UniFi driver */
    priv = register_unifi_sdio(func, instance, &func->dev);
    if (!priv) {
        sdio_release_irq(func);
        _sdio_release_host(func);
        func_exit();
        return -ENOMEM;
    }

    sdio_set_drvdata(func, priv);

    _sdio_release_host(func);

    func_exit();
    return 0;
} /* uf_sdio_probe() */


/*
 * ---------------------------------------------------------------------------
 *  uf_sdio_remove
 *
 *      Card removal callback.
 *
 * Arguments:
 *      fdev            SDIO device
 *
 * Returns:
 *      UniFi driver error code.
 * ---------------------------------------------------------------------------
 */
static void
uf_sdio_remove(struct sdio_func *func)
{
    func_enter();

    unifi_info(NULL, "card removed\n");

    _sdio_claim_host(func);
    sdio_release_irq(func);
    _sdio_release_host(func);

    /* Clean up the main UniFi driver */
    unregister_unifi_sdio(func->card->host->index);

    func_exit();

} /* uf_sdio_remove */




static const struct sdio_device_id unifi_ids[] = {
    { SDIO_DEVICE(SDIO_MANF_ID_CSR,SDIO_CARD_ID_UNIFI_1) },
    { SDIO_DEVICE(SDIO_MANF_ID_CSR,SDIO_CARD_ID_UNIFI_3) },
    { SDIO_DEVICE(SDIO_MANF_ID_CSR,SDIO_CARD_ID_UNIFI_4) },
    { /* end: all zeroes */				},
};

MODULE_DEVICE_TABLE(sdio, unifi_ids);

static struct sdio_driver unifi_driver = {
    .probe      = uf_sdio_probe,
    .remove     = uf_sdio_remove,
    .name       = "unifi",
    .id_table	= unifi_ids,
};


/*
 * ---------------------------------------------------------------------------
 *  uf_sdio_load
 *  uf_sdio_unload
 *
 *      These functions are called from the main module load and unload
 *      functions. They perform the appropriate operations for the monolithic
 *      driver.
 * 
 *  Arguments:
 *      None.
 *
 *  Returns:
 *      None.
 * ---------------------------------------------------------------------------
 */
int __init
uf_sdio_load(void)
{
    int r;

    printk("Unifi: Using native Linux MMC driver for SDIO\n");

    r = sdio_register_driver(&unifi_driver);
    if (r) {
        printk(KERN_ERR "unifi_sdio: Failed to register UniFi SDIO driver: %d\n", r);
        return r;
    }

    return 0;
} /* uf_sdio_load() */



void __exit
uf_sdio_unload(void)
{
    printk("unload sdio_mmc\n");
    sdio_unregister_driver(&unifi_driver);
} /* uf_sdio_unload() */

