/** @file osa.h
 *
 * Operating System Abstraction main header file
 *
 * @section LEGAL
 *   CONFIDENTIAL
 *
 *   Copyright (C) Cambridge Silicon Radio Ltd 2008. All rights reserved.
 *
 *   Refer to LICENSE.txt included with this source for details on the
 *   license terms.
 *
 * @section DESCRIPTION
 *   Provides abstraction API for typical OS-related functions such as
 *   malloc, free, etc.
 *
 ****************************************************************************
 *
 * @section REVISION
 *   $Id: //depot/dot11/v6.1/host/lib_sme/common/abstractions/osa.h#1 $
 *
 ****************************************************************************/
#ifndef OSA_H
#   define OSA_H

#ifdef __cplusplus
extern "C" {
#endif

/** @{
 * @ingroup abstractions
 */

/* STANDARD INCLUDES ********************************************************/

/* PROJECT INCLUDES *********************************************************/
#include "osa_types.h"

/* PUBLIC MACROS ************************************************************/
/**
 * This macro defines the behaviour of the system under a panic condition.
 * There are two available options: osa_panic_verbose and osa_panic_brief,
 * both of which are documented below.
 */
#define osa_panic(rc)   osa_panic_verbose(__FILE__, __LINE__, (rc))
/*#define osa_panic(rc)   osa_panic_brief(rc)*/

/**
 * This macro defines the number of milliseconds in a second
 */
#define MS_SEC (1000)

/**
 * This macro defines the number of microseconds in a second
 */
#define US_SEC ((1000 * 1000)UL)


/* PUBLIC TYPES DEFINITIONS *************************************************/
/**
 * @brief
 *   A panic reason code
 *
 * @par Description
 *   This provides the upper layers with a set of reason codes to use when
 *   calling the osa_panic function.
 */
typedef enum
{
    /** Memory exhaustion */
    OSA_PANIC_MEM_EXHAUST,
    /** Unable to initialise the osa */
    OSA_PANIC_OSA_INIT,
    /** Error marking start of critical section code */
    OSA_PANIC_CRITSEC_ENTRY,
    /** Error marking end of critical section code */
    OSA_PANIC_CRITSEC_EXIT,
    /** Panic unsupported operation occured */
    OSA_PANIC_UNSUPPORTED,
    /** Error in signal unpacking engine */
    OSA_PANIC_PACK_ERROR,
    /** Error in signal packing engine */
    OSA_PANIC_UNPACK_ERROR,
    /** Internal logic failure */
    OSA_PANIC_INTERNAL_LOGIC,
    /** Unknown or unspecified panic reason */
    OSA_PANIC_UNKNOWN
} osa_panic_code;

typedef struct OsaMutex OsaMutex;

/* PUBLIC CONSTANT DECLARATIONS *********************************************/

/* PUBLIC VARIABLE DECLARATIONS *********************************************/

/* PUBLIC FUNCTION PROTOTYPES ***********************************************/

/**
 * @brief
 *   Initialises the osa
 *
 * @par Description
 *   Initialises and Data needed by the osa for use
 *   This function should only be called ONCE before any other osa functions are
 *   called.
 *
 * @return
 *   void
 *
 * @remarks
 *   Calls osa_panic on any failure
 * @par
 *   The exact specification of the requirements for this function depend on
 *   the threading model used on the target platform. Please refer to the SME
 *   Porting guide for full details.
 *
 * @par
 *   See also osa_shutdown().
 */
extern void osa_initialise(void);

/**
 * @brief
 *   Frees any resources allocated by osa_initialise
 *
 * @par Description
 *   This function is called ONCE prior to complete termination of the
 *   program.  It should free any resources allocated by osa_initialise.
 *
 * @return
 *   void
 *
 * @remarks
 *   Failures must not cause this function to terminate prematurely; it
 *   should attempt to free any resources owned by osa_initialise.
 * @par
 *   The exact specification of the requirements for this function depend on
 *   the threading model used on the target platform. Please refer to the SME
 *   Porting guide for full details.
 */
extern void osa_shutdown(void);

/**
 * @brief
 *   Abnormal program termination mechanism with verbose 'output'
 *
 * @par Description
 *   Allows software to cause an abnormal program termination, possibly
 *   resulting in a hardware reset or service-affecting recovery
 *   mechanism. It should be called whenever a critical error that cannot
 *   be handled by normal (software) mechanisms occurs, for example a
 *   failure to obtain heap resources.
 * @par
 *   This function does not return control to the calling context but when
 *   feasable the panic code passed via argument 3 is exported to the executive
 *   environment under which the SME was spawned by calling, for example,
 *   exit(reason).
 * @par
 *   Either this function or osa_panic_brief() will be mapped to be the
 *   expansion for the osa_panic macro. The implementation provided by this
 *   function attempts to output (via sme_trace) the source file name and
 *   line number where the osa_panic macro was invoked in addition to the
 *   panic code (reason).
 *
 * @param[in] filename : The name of the file where panic was called
 * @param[in] lineNum  : The line in the file where panic was called
 * @param[in] reason   : The reason for the panic (panicCode type)
 *
 * @return
 *   NONE - this function never returns
 *
 * @remarks
 *   osa_panic will be macro'd to either this function or osa_panic_brief
 */
extern void osa_panic_verbose(const char * filename,
                              int lineNum,
                              osa_panic_code reason) OSA_NORETURN;

/**
 * @brief
 *   Abnormal program termination mechanism with brief 'output'
 *
 * @par Description
 *   Allows software to cause an abnormal program termination, possibly
 *   resulting in a hardware reset or service-affecting recovery
 *   mechanism. It should be called whenever a critical error that cannot
 *   be handled by normal (software) mechanisms occurs, for example a
 *   failure to obtain heap resources.
 * @par
 *   This function does not return control to the calling context but when
 *   feasable the panic code passed via argument 3 is exported to the executive
 *   environment under which the SME was spawned by calling, for example,
 *   exit(reason).
 * @par
 *   Either this function or osa_panic_verbose() will be mapped to be the
 *   expansion for the osa_panic macro. The implementation provided by this
 *   function attempts to output (via sme_trace) the panic code (reason).
 *
 * @param[in]     reason  : The reason for the panic (panicCode type)
 *
 * @return
 *   NONE - this function never returns
 *
 * @remarks
 *   osa_panic will be macro'd to either this function or osa_panic_verbose
 */
extern void osa_panic_brief(osa_panic_code reason) OSA_NORETURN;

/**
 * @brief
 *   malloc() abstraction
 *
 * @par Description
 *   This function, like its ANSI C Library malloc() equivalent, allocates sz
 *   bytes of contiguous memory and returns a pointer to the start of the
 *   allocated memory. The memory is not required to be cleared.
 * @par
 *   Additionally, osa_malloc() needs to record the length of the block
 *   associated with the returned address so that any calls to osa_msizeof()
 *   function are correctly serviced.
 *
 * @param[in]     sz      : The amount of memory to allocate in bytes
 *
 * @return
 *    void *: pointer to the allocated memory block
 *
 * @remarks
 *   Unlike malloc() if the allocation request cannot be satisfied, this
 *   function calls osa_panic() with an osa_panic_code of OSA_PANIC_MEM_EXHAUST.
 *   A NULL pointer is never returned.
 *
 * @pre  sz > 0
 *
 * @post
 *   returns pointer to contiguous block of memory of at least size (sz) bytes
 */
#define osa_malloc(sz) malloc(sz)

/**
 * @brief
 *   calloc() abstraction
 *
 * @par Description
 *   This function, like its ANSI C Library calloc() equivalent, behaves similarly
 *   to the osa_malloc() function, except that the size of the memory block is given
 *   by (nmemb sz) where nmemb is the number of members and that the allocated block
 *   has its content cleared (initialised to zero).
 *
 * @param[in]     nmemb   : Number of members (units) to allocate
 * @param[out]    sz      : The size of each member in bytes
 *
 * @return
 *   void *: pointer to the allocated memory block
 *
 * @see osa_malloc
 *
 * @remarks
 *   Like osa_malloc, a failure to perform the allocation request, results in
 *   this function calling osa_panic() with an osa_panic_code of
 *   OSA_PANIC_MEM_EXHAUST. A NULL pointer is never returned.
 * @par
 *   May rely on functionality provided by osa_malloc. If it does not, the
 *   function must ensure that it records the length of the block associated
 *   with the returned address so that any calls to osa_msizeof() are correctly
 *   serviced.
 *
 * @pre  nmemb > 0
 * @pre  sz > 0
 *
 * @post returns pointer to contiguous block of memory of at least size (sz) bytes
 *       whose contents have been initialised to zero.
 */
#define osa_calloc(nmemb, sz) calloc(nmemb, sz)

/**
 * @brief
 *   free() abstraction.
 *
 * @par Description
 *   This function, like its ANSI C free() equivalent, releases the memory
 *   referenced by ptr. It assumes that the addresses referenced by ptr was
 *   returned by an earlier call to either osa_malloc(), or osa_calloc().
 *
 * @param[in]     ptr     : Pointer to the mem block that is no longer needed
 *
 * @return
 *   void
 *
 * @remarks
 *   Calls to osa_free() with ptr containing an address not returned by a
 *   prior call to osa_malloc() or osa_calloc() results in undefined behaviour.
 * @par
 *   Will accept (ignore) an attempt to free a NULL pointer
 *
 * @pre  ptr must reference a valid block of memory allocated previously using
 *       osa_malloc or osa_calloc.
 */
#define osa_free(ptr) free(ptr)

/**
 * @brief
 *   Creates a critical section
 *
 * @par Description
 *   See brief
 *
 * @return
 *   OsaMutex;
 *
 * @remarks
 *   Calls osa_panic on any failure
 */
extern OsaMutex* osa_critical_section_create(void);

/**
 * @brief
 *   Marks the begining of a critical section of code at which point no other
 *   execution context may get to run.
 *
 * @par Description
 *   See brief
 *
 * @return
 *   void
 *
 * @remarks
 *   Calls osa_panic on any failure
 * @par
 *   The exact specification of the requirements for this function depend on
 *   the threading model used on the target platform. Please refer to the SME
 *   Porting guide for full details.
 *
 * @pre  hdl must be a valid critical section
 */
extern void osa_critical_section_entry(OsaMutex* mutex);

/**
 * @brief
 *   Marks the end of a critical section of code at which point other
 *   execution contexts may be allowed to run the previously protected code.
 *
 * @par Description
 *   See brief
 *
 * @return
 *   void
 *
 * @remarks
 *   Calls osa_panic on any failure

 * @par
 *   The exact specification of the requirements for this function depend on
 *   the threading model used on the target platform. Please refer to the SME
 *   Porting guide for full details.
 *
 * @pre  hdl must be a valid critical section
 */
extern void osa_critical_section_exit(OsaMutex* mutex);

/**
 * @brief
 *   Destroys a critical section
 *
 * @par Description
 *   See brief
 *
 * @return
 *   void
 *
 * @remarks
 *   Calls osa_panic on any failure
 */
extern void osa_critical_section_destroy(OsaMutex* mutex);

/**
 * @brief
 *   Returns the time of day in milliseconds.
 *
 * @par Description
 *   This function returns a representation of 'current time' that is held
 *   in a timer whose value increases in units of milliseconds. This value
 *   wraps through zero across the 32-bit boundary.
 *
 * @return
 *   uint32: millisecond resolution timer value
 *
 * @remarks
 *   The timer need not have resolution of 1 millisecond, i.e. it may increment
 *   by 'n' every 'n' milliseconds.
 *
 * @post The returned value must simulate a timer that increments by 1 every
 *       millisecond and wraps to zero at 2^32.
 */
extern uint32 osa_get_time_of_day_milli_seconds(void);

#ifdef SME_TEST_SUPPORT
/**
 * @brief
 *   Advances the time of day
 *
 * @par Description
 *   This function adds an offset to the time reported by
 *   osa_get_time_of_day_milli_seconds(). The offset is cumulative so
 *   increases with subsequent calls to osa_advance_time().
 *
 * @return
 *   void
 */
extern void osa_advance_time(uint32 ms);
#endif

/**
 * @brief
 *   Populate a buffer with random data
 *
 * @par Description
 *   Support
 */
extern void osa_random(uint8* buff, uint32 length);

/**
 * @brief
 *   Compare two strings
 *
 * @par Description
 *   Provides identical functionality to the ANSI C 'strncmp'
 */
#define osa_strncmp(s1, s2, n) strncmp(s1, s2, n)

/**
 * @brief
 *   Copy a string into a pre-allocated buffer
 *
 * @par Description
 *   Provides identical functionality to the ANSI C 'strcpy'
 */
#define osa_strcpy(dest, src) strcpy(dest, src)

/**
 * @brief
 *   Concatenates a string onto the end of another string in a pre-allocated
 *   buffer
 *
 * @par Description
 *   Provides identical functionality to the ANSI C 'strcat'
 */
#define osa_strcat(dest, src) strcat(dest, src)


/**
 * @brief
 *   Gives the number of bytes in a string, not including the zero terminator
 *
 * @par Description
 *   Provides identical functionality to the ANSI C 'strlen'
 */
#define osa_strlen(s) strlen(s)

/**
 * @brief
 *   Fills an area of memory with the given 8-bit value
 *
 * @par Description
 *   Provides identical functionality to the ANSI C 'memset'
 */
#define osa_memset(s, c, n) memset(s, c, n)

/**
 * @brief
 *   Copies an area of memory to a pre-allocated buffer
 *
 * @par Description
 *   Provides identical functionality to the ANSI C 'memcpy'
 */
#define osa_memcpy(dest, src, n) memcpy(dest, src, n)

/**
 * @brief
 *   Moves an data from one area of memory to another.  The source and
 *   destination areas may overlap.
 *
 * @par Description
 *   Provides identical functionality to the ANSI C 'memmove'
 */
#define osa_memmove(dest, src, n) memmove(dest, src, n)

/**
 * @brief
 *   Compare two areas of memory
 *
 * @par Description
 *   Provides identical functionality to the ANSI C 'memcmp'
 */
#define osa_memcmp(s1, s2, n) memcmp(s1, s2, n)

/**
 * @brief
 *   Convert a string to an unsigned long value
 *
 * @par Description
 *   Provides identical functionality to the ANSI C 'strtoul'
 */
#define osa_strtoul(nptr, endptr, base) strtoul(nptr, endptr, base)

/**
 * @brief
 *   Printf into a string
 *
 * @par Description
 *   Provides identical functionality to the ANSI C 'sprintf'
 */
#define osa_sprintf sprintf

/**
 * @brief
 *   assert function
 *
 * @par Description
 *   Provides identical functionality to linux <assert.h> 'assert'
 */
#define osa_assert assert


/* OVERRIDE AND IMPLEMENTATION INCLUDES *********************************************************/
#include "osa_support.h"


/** @}
 */

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* OSA_H */
