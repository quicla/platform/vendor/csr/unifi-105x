/** @file event_pack_unpack.c
 *
 * event Packing and unpacking functions
 *
 * @section LEGAL
 *   CONFIDENTIAL
 *
  *   Copyright (C) Cambridge Silicon Radio Ltd 2008. All rights reserved.
 *
 *   Refer to LICENSE.txt included with this source for details on the
 *   license terms.
 *
 * @section DESCRIPTION
 *   Implements Event Packing and Unpacking of signals for compatability
 *   across platforms. different Endianness and Native Packing schemes need
 *   to be supported.
 *
 ****************************************************************************
 *
 * @section REVISION
 *   $Id: //depot/dot11/v6.1/host/lib_sme/common/event_pack_unpack/event_pack_unpack.c#1 $
 *
 ****************************************************************************/
/** @ingroup common
 * @{
 */

/* STANDARD INCLUDES ********************************************************/

/* PROJECT INCLUDES *********************************************************/
#include "abstractions/osa.h"
#include "event_pack_unpack/event_pack_unpack.h"

/* MACROS *******************************************************************/

/* GLOBAL VARIABLE DEFINITIONS **********************************************/

/* PRIVATE TYPES DEFINITIONS ************************************************/

/* PRIVATE CONSTANT DEFINITIONS *********************************************/

/* PRIVATE VARIABLE DEFINITIONS *********************************************/

/* PRIVATE FUNCTION PROTOTYPES **********************************************/

/* PRIVATE FUNCTION DEFINITIONS *********************************************/

/* PUBLIC FUNCTION DEFINITIONS **********************************************/

/**
 * See description in event_pack_unpack/event_pack_unpack.h
 */
uint16 event_pack_uint8 (uint8** resultBuffer, uint8  value)
{
    (*resultBuffer)[0] = value;
    (*resultBuffer)++;
    return 1;
}

/**
 * See description in event_pack_unpack/event_pack_unpack.h
 */
uint16 event_pack_uint16(uint8** resultBuffer, uint16 value)
{
    (*resultBuffer)[0] =  value        & 0xFF;
    (*resultBuffer)[1] = (value >>  8) & 0xFF;
    (*resultBuffer) += 2;
    return 2;
}

/**
 * See description in event_pack_unpack/event_pack_unpack.h
 */
uint16 event_pack_uint32(uint8** resultBuffer, uint32 value)
{
    (*resultBuffer)[0] = (uint8)value         & 0xFF;
    (*resultBuffer)[1] = (uint8)(value >>  8) & 0xFF;
    (*resultBuffer)[2] = (uint8)(value >> 16) & 0xFF;
    (*resultBuffer)[3] = (uint8)(value >> 24) & 0xFF;
    (*resultBuffer) += 4;
    return 4;
}

/**
 * See description in event_pack_unpack/event_pack_unpack.h
 */
uint16 event_pack_buffer(uint8** resultBuffer, uint8* value, uint16 numberOfBytes)
{
    osa_memmove(*resultBuffer, value, numberOfBytes);
    (*resultBuffer) += numberOfBytes;
    return numberOfBytes;
}

/**
 * See description in event_pack_unpack/event_pack_unpack.h
 */
uint16 event_pack_string(uint8** resultBuffer, unifi_String value)
{
    uint16 len = (uint16)osa_strlen(value) + 1;
    (void)event_pack_int16(resultBuffer, (int16)len);
    osa_memmove(*resultBuffer, value, len);
    (*resultBuffer) += len;
    return len + 2;
}


/**
 * See description in event_pack_unpack/event_pack_unpack.h
 */
uint8 event_unpack_uint8 (uint8** signalBuffer)
{
    uint8 result = (*signalBuffer)[0];
    (*signalBuffer)++;
    return result;
}

/**
 * See description in event_pack_unpack/event_pack_unpack.h
 */
uint16 event_unpack_uint16(uint8** signalBuffer)
{
    uint16 result = ((*signalBuffer)[1] << 8) |
                     (*signalBuffer)[0];
    (*signalBuffer)+=2;
    return result;
}

/**
 * See description in event_pack_unpack/event_pack_unpack.h
 */
uint32 event_unpack_uint32(uint8** signalBuffer)
{
    uint32 result = ((*signalBuffer)[3] << 24) |
                    ((*signalBuffer)[2] << 16) |
                    ((*signalBuffer)[1] <<  8) |
                     (*signalBuffer)[0];
    (*signalBuffer)+=4;
    return result;
}

/**
 * See description in event_pack_unpack/event_pack_unpack.h
 */
void event_unpack_buffer(uint8** signalBuffer, uint8* resultBuffer, uint16 numberOfBytes)
{
    osa_memmove(resultBuffer, *signalBuffer, numberOfBytes);
    (*signalBuffer) += numberOfBytes;
}

/**
 * See description in event_pack_unpack/event_pack_unpack.h
 */
unifi_String event_unpack_string(uint8** signalBuffer)
{
    unifi_String result = NULL;
    uint16 len = (uint16)event_unpack_int16(signalBuffer);
    if (len > 0)
    {
        result = (unifi_String)osa_malloc(len);
        if (result) {
            osa_memmove(result, *signalBuffer, len);
            (*signalBuffer) += len;
        }
    }

    return result;
}


uint16 event_pack_hip_header(uint8** signalBuffer, uint16 id, uint16 pid, const DataReference* dataref1, const DataReference* dataref2)
{
    (void)event_pack_uint16(signalBuffer, id);
    (void)event_pack_uint16(signalBuffer, 0);
    (void)event_pack_uint16(signalBuffer, pid);
    if (dataref1)
    {
        (void)event_pack_uint16(signalBuffer, dataref1->slotNumber);
        (void)event_pack_uint16(signalBuffer, dataref1->dataLength);
    }
    else
    {
        (void)event_pack_uint32(signalBuffer, 0);
    }

    if (dataref2)
    {
        (void)event_pack_uint16(signalBuffer, dataref2->slotNumber);
        (void)event_pack_uint16(signalBuffer, dataref2->dataLength);
    }
    else
    {
        (void)event_pack_uint32(signalBuffer, 0);
    }
    return 14;
}


/** @}
 */
